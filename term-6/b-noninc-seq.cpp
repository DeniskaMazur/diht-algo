#include <functional>
#include <iostream>
#include <set>
#include <vector>

using std::pair;

typedef long long ll;

std::vector<size_t> compute_lnis(const std::vector<ll>& seq) {
    const size_t n = seq.size();

    auto cmp = [](const pair<ll, size_t>& lhs, const pair<ll, size_t>& rhs) {
        return lhs.first > rhs.first;
    };

    std::multiset<pair<ll, size_t>, decltype(cmp)> noninc(cmp);

    std::vector<int> prevs(n, -1);
    for (size_t i = 0; i < n; i++) {
        auto it = noninc.insert({seq[i], i});

        if (it != noninc.begin())
            prevs[i] = std::prev(it)->second;
        if (std::next(it) != noninc.end())
            noninc.erase(std::next(it));
    }

    std::vector<size_t> answ;
    answ.push_back(noninc.rbegin()->second);
    while (prevs[answ.back()] != -1)
        answ.push_back(prevs[answ.back()]);

    std::reverse(answ.begin(), answ.end());

    return answ;
}

int main() {
    size_t n;
    std::cin >> n;

    std::vector<ll> seq(n);
    for (ll& e : seq)
        std::cin >> e;

    auto result = compute_lnis(seq);
    std::cout << result.size() << std::endl;

    for (auto it = result.begin(); it != result.end(); ++it)
        std::cout << *it + 1 << " ";
}

