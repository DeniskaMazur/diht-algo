#include <iostream>
#include <vector>

int solve_knapsack(const std::vector<int>& weights, size_t capacity) {
    const size_t n = weights.size();

    std::vector<std::vector<int>> dp(capacity + 1, std::vector<int>(n + 1, 0));

    for (size_t i = 1; i <= n; i++) {
        for (int j = 1; j <= capacity; j++) {
            if (weights[i - 1] <= j) {
                dp[j][i] =
                    std::max(dp[j][i - 1],
                             dp[j - weights[i - 1]][i - 1] + weights[i - 1]);
            } else {
                dp[j][i] = dp[j][i - 1];
            }
        }
    }

    return dp[capacity][n];
}

int main() {
    int s, n;
    std::cin >> s >> n;

    std::vector<int> weights(n);
    for (auto& w : weights)
        std::cin >> w;

    std::cout << solve_knapsack(weights, s) << std::endl;
}

