#include <iostream>
#include <vector>
 
 
size_t lcs(const std::vector<int>& x, const std::vector<int>& y) {
    std::vector<size_t> curr(y.size() + 1);
 
    for (size_t i = 1; i <= x.size(); i++) {
        auto prev = curr;
        for (size_t j = 1; j <= y.size(); j++) {
            if (x[i - 1] == y[j - 1])
                curr[j] = prev[j] + 1;
            else if (prev[j] >= curr[j - 1])
                curr[j] = prev[j];
            else
                curr[j] = curr[j - 1];
        }
    }
 
    return curr.back();
}
 
int main() {
    size_t n, m;
 
    std::cin >> n;
    std::vector<int> x(n);
    for (int& e: x)
        std::cin >> e;
 
    std::cin >> m;
    std::vector<int> y(m);
    for (int& e: y)
        std::cin >> e;
 
    std::cout << lcs(x, y) << std::endl;
}

