#include <algorithm>
#include <iostream>
#include <vector>

size_t compute_len_gcss(const std::vector<int>& fst,
                        const std::vector<int>& snd) {
    std::vector<std::vector<size_t>> dp(fst.size() + 1,
                                        std::vector<size_t>(snd.size() + 1));

    for (size_t i = 1; i <= fst.size(); i++) {
        int best = 0;
        for (size_t j = 1; j <= snd.size(); j++) {
            dp[i][j] = dp[i - 1][j];
            if (fst[i - 1] == snd[j - 1] and dp[i - 1][j] < best + 1) {
                dp[i][j] = best + 1;
            }
            if (fst[i - 1] > snd[j - 1] and dp[i - 1][j] > best) {
                best = dp[i - 1][j];
            }
        }
    }

    size_t best = 0;
    for (size_t i = 1; i <= fst.size(); i++)
        best = std::max(best, *std::max_element(dp[i].begin(), dp[i].end()));

    return best;
}

int main() {
    size_t n, k;
    std::cin >> n >> k;

    std::vector<int> fst(n), snd(k);
    for (int& e : fst)
        std::cin >> e;
    for (int& e : snd)
        std::cin >> e;

    std::cout << compute_len_gcss(fst, snd) << std::endl;
}

