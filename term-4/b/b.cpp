#include <functional>
#include <iostream>
#include <vector>

template <typename T>
class SegTree {
    friend int main();

    std::vector<T> backing;
    std::function<T(T, T)> func;
    T id;
    size_t n;

  public:
    SegTree(const std::vector<T> input, std::function<T(T, T)> func, T id) :
        func(func), id(id) {
        n = input.size();
        backing.resize(2 * n, id);

        for (size_t i = 0; i < n; ++i)
            backing[n + i] = input[i];

        for (size_t i = n - 1; i > 0; --i)
            backing[i] = func(backing[i << 1], backing[i << 1 | 1]);
    }

    void update(size_t p, T value) {
        for (backing[p += n] = value; p >>= 1;)
            backing[p] = func(backing[p << 1], backing[p << 1 | 1]);
    }

    T query(size_t l, size_t r) {
        T resl = id, resr = id;

        if (l == r)
            return backing[n + l];

        l += n;
        r += n + 1;

        for (; l < r; l >>= 1, r >>= 1) {
            if (l & 1)
                resl = func(resl, backing[l++]);
            if (r & 1)
                resr = func(backing[--r], resr);
        }

        return func(resl, resr);
    }
};

inline int sum(int a, int b) { return a + b; }

int main() {
    size_t n;
    std::cin >> n;

    std::vector<int> even_pos;
    std::vector<int> odd_pos;

    for (size_t i = 0; i < n; i++) {
        int x;
        std::cin >> x;

        if (i % 2 == 0)
            even_pos.push_back(x);
        else
            odd_pos.push_back(x);
    }

    SegTree<int> evens(even_pos, sum, 0);
    SegTree<int> odds(odd_pos, sum, 0);

    std::cin >> n;
    for (size_t i = 0; i < n; i++) {
        int command, left, right;
        std::cin >> command >> left >> right;

        switch (command) {
        case 0: {
            left--;

            if (left % 2 == 0) {
                left /= 2;
                evens.update(left, right);
            } else {
                left = (left - 1) / 2;
                odds.update(left, right);
            }

            break;
        }

        case 1: {
            left--;
            right--;

            int even_l, even_r, odd_l, odd_r;
            int even_sum, odd_sum;

            if (left == right) {
                if (left % 2 == 0)
                    std::cout << evens.query(left, right);
                else
                    std::cout << odds.query(left, right);
                continue;
            }

            even_l = (left + 1) / 2;
            even_r = right / 2;

            odd_l = left / 2;
            odd_r = (right - 1) / 2;

            even_sum = evens.query(even_l, even_r);
            odd_sum = odds.query(odd_l, odd_r);

            // std::cout << "even: " << "left: " << even_l << " right: " <<
            // even_r << " " << even_sum << std::endl; std::cout << "odd: "  <<
            // "left: " << odd_l  << " right: " << odd_r  << " " << odd_sum  <<
            // std::endl;

            if (left % 2 == 0)
                std::cout << even_sum - odd_sum << std::endl;
            else
                std::cout << odd_sum - even_sum << std::endl;
        }
        }
    }
}

