#include <algorithm>
#include <cmath>
#include <iostream>
#include <tuple>
#include <vector>

typedef long long ll;

template <typename T>
class Matrix {
    size_t dim1, dim2;
    // std::vector<T> backing;
    std::vector<std::vector<T>> backing;

  public:
    /*
    Matrix(size_t dim1, size_t dim2)
        : dim1(dim1), dim2(dim2), backing(dim1 * dim2) {}

    Matrix(size_t dim1, size_t dim2, T init)
        : dim1(dim1), dim2(dim2), backing(dim1 * dim2, init) {}

    T &operator()(const size_t &i, const size_t &j) {
        return backing[dim1 * i + j];
    }
    const T &operator()(const size_t &i, const size_t &j) const {
        return backing[dim1 * i + j];
    }
    */

    Matrix(size_t dim1, size_t dim2, T init) : dim1(dim1), dim2(dim2) {
        backing = std::vector<std::vector<T>>(dim1, std::vector<T>(dim2));
    }

    T& operator()(const size_t& i, const size_t& j) { return backing[i][j]; }
    const T& operator()(const size_t& i, const size_t& j) const {
        return backing[i][j];
    }

    size_t nrows() const { return dim1; };
    size_t ncols() const { return dim2; };
};

class OrderStatST {
    size_t size;
    std::vector<ll> initial;
    std::vector<size_t> logs;
    Matrix<std::pair<ll*, ll*>> backing;

    static std::pair<ll*, ll*> weird_min(std::pair<ll*, ll*> lhs,
                                         std::pair<ll*, ll*> rhs) {
        std::array<ll*, 4> tobesorted = {
            lhs.first, lhs.second, rhs.first, rhs.second};
        std::sort(tobesorted.begin(), tobesorted.end(), [](ll*& lhs, ll*& rhs) {
            return *lhs < *rhs;
        });

        ll* second;
        for (size_t i = 1; i < 4; i++)
            if (tobesorted[i] != tobesorted[0]) {
                second = tobesorted[i];
                break;
            }

        return std::make_pair(tobesorted[0], second);
    }

  public:
    OrderStatST(const std::vector<ll>& initial_) :
        size(initial_.size()), initial(initial_),
        backing(static_cast<size_t>(ceil(log2(initial.size()))),
                initial.size(),
                std::make_pair(nullptr, nullptr)) {

        for (size_t i = 0; i < size; i++) {
            backing(0, i).first = &initial[i];
            backing(0, i).second = &initial[i];
        }

        for (size_t i = 1; i < backing.nrows(); i++) {
            for (size_t j = 0; j < size - (1 << i) + 1; j++) {
                std::pair<ll*, ll*> min_pair = weird_min(
                    backing(i - 1, j), backing(i - 1, j + (1 << (i - 1))));
                backing(i, j).first = min_pair.first;
                backing(i, j).second = min_pair.second;
            }
        }

        for (size_t i = 0; i <= size; i++)
            logs.push_back(static_cast<size_t>(ceil(log2(i))));
    }

    ll eval(size_t begin, size_t end) const {
        size_t k = logs[end - begin + 1] - 1;
        return *weird_min(backing(k, begin), backing(k, end - (1 << k) + 1))
                    .second;
    }
};

int main() {
    size_t n, m;
    std::cin >> n >> m;

    std::vector<ll> seq(n);
    for (auto& e : seq)
        std::cin >> e;

    OrderStatST table(seq);

    for (size_t i = 0; i < m; i++) {
        size_t from, to;
        std::cin >> from >> to;
        std::cout << table.eval(from - 1, to - 1) << std::endl;
    }
}
 
