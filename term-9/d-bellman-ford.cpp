#include <cstddef>
#include <iostream>
#include <memory.h>
#include <vector>
#include <climits>
#include <cmath>
 
typedef long long ll;
ll INF = pow(10, 16);
 
struct node {
    size_t to;
    ll dist;
 
    node(size_t to, ll dist): to(to), dist(dist) {};
};
 
class Graph {
    std::vector<std::vector<node>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}
 
    void add_edge(size_t from, size_t to, ll dist) { adj[from].push_back({to, dist}); }
    size_t size() const { return adj.size(); }
 
    const std::vector<node>& adj_to(size_t v) const { return adj[v]; }
};
 
void bellman_ford(size_t s, std::vector<ll>& d, const Graph& g) {
    const size_t n = g.size() - 1;
    d[s] = 0;
 
    for (size_t i = 1; i <= n; i++) {
        for (size_t j = 1; j <= n; j++) {
            for (size_t k = 0; k < g.adj_to(j).size(); k++) {
                size_t to = g.adj_to(j)[k].to;
                ll dist = g.adj_to(j)[k].dist;
 
                if ((d[j] < INF) && (d[to] > d[j] + dist))
                    d[to] = d[j] + dist;
            }
        }
    }
}
 
void dfs(size_t v, std::vector<bool>& used, const Graph& g) {
    used[v] = true;
    for (size_t i = 0; i < g.adj_to(v).size(); i++) {
        size_t to = g.adj_to(v)[i].to;
        if (!used[to])
            dfs(to, used, g);
    }
}
 
int main() {
    size_t n, m, s;
    std::cin >> n >> m >> s;
 
    Graph graph(n + 1);
    for (size_t i = 0; i < m; i++) {
        size_t u, v, w;
        std::cin >> u >> v >> w;
 
        graph.add_edge(u, v, w);
    }
 
    std::vector<ll> d(n + 1, INF);
    bellman_ford(s, d, graph);
    std::vector<bool> used(n);
 
    for (size_t i = 1; i <= n; i++) {
        for (size_t j = 0; j < graph.adj_to(i).size(); j++) {
            size_t to = graph.adj_to(i)[j].to;
            ll dist = graph.adj_to(i)[j].dist;
            if ((d[i] < INF) && (d[to] > d[i] + dist) && !used[to])
                dfs(to, used, graph);
        }
    }
 
    for (size_t i = 1; i <= n; i++) {
        if (d[i] == INF)
            std::cout << '*' << std::endl;
        else if (used[i])
            std::cout << '-' << std::endl;
        else
            std::cout << d[i] << std::endl;
    }
}