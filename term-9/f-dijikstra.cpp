#include <cstddef>
#include <iostream>
#include <utility>
#include <vector>
#include <set>
#include <queue>
 
typedef long long ll;
const ll kInf = 1e18;
 
struct edge {
    size_t to;
    ll dist;
 
    explicit edge(size_t to, ll dist): to(to), dist(dist) {}
};

class Graph {
    std::vector<std::vector<edge>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}
 
    void add_edge(size_t from, size_t to, ll dist) { adj[from].push_back(edge(to, dist)); }
    size_t size() const { return adj.size(); }
 
    const std::vector<edge>& adj_to(size_t v) const { return adj[v]; }
};
 
ll dijkstra(const Graph& graphList, size_t root, size_t to) {
    const size_t n = graphList.size();
 
    std::vector<ll> dist(n, kInf);
    std::set<std::pair<ll, ll>> heap;
 
    dist[root] = 0;
    heap.insert(std::make_pair(dist[root], root));
 
    while (!heap.empty()) {
        size_t u = heap.begin()->second;
        heap.erase(heap.begin());
        for (const edge& p: graphList.adj_to(u)) {
            ll v = p.to;
            ll cost = p.dist;
            if (dist[u] + cost < dist[v]) {
                heap.erase(std::make_pair(dist[v], v));
                dist[v] = dist[u] + cost;
                heap.insert(std::make_pair(dist[v], v));
            }
        }
    }
 
    return dist[to];
}
 
int main() {
    size_t n, m, from, to;
    std::cin >> n >> m >> from >> to;
 
    Graph graph(n);
    for (size_t i = 0; i < m; i++) {
        size_t u, v;
        ll dist;
        std::cin >> u >> v >> dist;
 
        graph.add_edge(u - 1, v - 1, dist);
        graph.add_edge(v - 1, u - 1, dist);
    }
 
    ll result = dijkstra(graph, from - 1, to - 1);
    std::cout << (result == kInf ? -1 : result) << std::endl;
}