#include "stdlib.h"
#include "stdio.h"

typedef long long ll;

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

ll min_max(ll a[], ll b[], ll len) {
    ll l = 0;
    ll r = len - 1;

    while (r - l > 1) {
        int mid = (l + r) / 2;
        if (a[mid] <= b[mid]) {
            l = mid;
        } else {
            r = mid;
        }
    }

    if (max(a[r], b[r]) < max(a[l], b[l])) {
        return r;
    } else {
        return l;
    }
}

int main() {
    ll n, m, l;
    scanf("%lld%lld%lld", &n, &m, &l);

    ll asc[n][l];
    for (ll i= 0; i < n; i++) {
        for (ll j = 0; j < l; j++) {
            scanf("%lld", &asc[i][j]);
        }
    }

    ll des[m][l];
    for (ll i= 0; i < m; i++) {
        for (ll j = 0; j < l; j++) {
            scanf("%lld", &des[i][j]);
        }
    }

    ll q;
    scanf("%lld", &q);

    for (ll k = 0; k < q; k++) {
        ll i, j;
        scanf("%lld %lld", &i, &j);
        printf("%lld\n", min_max(asc[i - 1], des[j - 1], l) + 1);
    }
}
