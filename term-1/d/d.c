/*
 * Гоблины Мглистых гор очень любях ходить к своим шаманам. Так как гоблинов много, к шаманам часто образуются очень
 * длинные очереди. А поскольку много гоблинов в одном месте быстро образуют шумную толку, которая мешает шаманам
 * проводить сложные медицинские манипуляции, последние решили установить некоторые правила касательно порядка в очереди.

 * Обычные гоблины при посещении шаманов должны вставать в конец очереди. Привилегированные же гоблины, знающие особый
 * пароль, встают ровно в ее середину, причем при нечетной длине очереди они встают сразу за центром.

 * Так как гоблины также широко известны своим непочтительным отношением ко всяческим правилам и законам, шаманы
 * попросили вас написать программу, которая бы отслеживала порядок гоблинов в очереди.
 */

#include "stdlib.h"
#include "stdio.h"
#include "assert.h"
#include "string.h"

typedef struct Node Node;
typedef int NodeType;
struct Node {
    NodeType value;
    Node *next;
};

typedef struct Queue Queue;
struct Queue {
    Node *head;
    Node *tail;
    Node *midl;

    size_t size;
};

// o - o - o - o + o
//     ^ move the middle pointer in tail direction if pre-enqueue size is even
void enqueue(Queue *this, NodeType value) {
    Node *node = malloc(sizeof(Node));
    node->value = value;
    node->next = NULL;

    if (this->size == 0) {
        this->head = node;
        this->tail = node;
        this->midl = node;

        this->size++;
        return;
    }

    this->tail->next = node;
    this->tail = node;

    this->size++;

    // move middle pointer if necessary
    if (this->size % 2 == 1) {
        this->midl = this->midl->next;
    }

    return;
}

void priority_enqueue(Queue *this, int value) {
    if (this->size <= 1) {
        enqueue(this, value);
        return;
    }

    Node *node = malloc(sizeof(Node));
    node->value = value;

    node->next = this->midl->next;
    this->midl->next = node;

    this->size++;

    if (this->size % 2 == 1) {
        this->midl = node;
    }
}

NodeType dequeue(Queue *this) {
    assert(this->size > 0);

    NodeType value = this->head->value;
    Node *tmp = this->head;

    if (this->size == 1) {
        free(tmp);
        this->tail = this->midl = this->head = NULL;

        this->size--;
        return value;
    }

    if (this->size % 2 == 0) {
        this->midl = this->midl->next;
    }

    this->head = this->head->next;
    free(tmp);

    this->size--;
    return value;
}

int main() {
    Queue queue;
    queue.size = 0;
    queue.head = queue.tail = queue.midl = NULL;

    int n;
    scanf("%d", &n);

    int count = 0;
    while (count < n) {
        char c;
        scanf("%c", &c);
        if (c == '-') {
            printf("%d\n", dequeue(&queue));
            count += 1;
        } else if (c == '+') {
            int t;
            scanf("%d", &t);
            enqueue(&queue, t);
            count += 1;
        } else if (c == '*') {
            int t;
            scanf("%d", &t);
            priority_enqueue(&queue, t);
            count += 1;
        }
    }
}
