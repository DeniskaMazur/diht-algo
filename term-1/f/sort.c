/*
 * Напишите программу, которая для заданного массива A=⟨a1,a2,…,an⟩ находит количество пар (i,j) таких, что i<j и ai>aj.
 * Обратите внимание на то, что ответ может не влезать в int.
 */

#include <stdio.h>
#include <stdlib.h>

typedef long long ll;

ll merge(int* orig, int start, int middle, int end) {
    ll count = 0;

    // allocate buffer array
    int *temp = (int *) malloc((end + 1) * sizeof(int));

    // copy original array into buffer array
    int i, j;
    for (i = start; i <= middle; i++) {
        temp[i] = orig[i];
    }
    for (j = end; i <= end; i++, j--) {
        temp[i] = orig[j];
    }

    // merge left and right arrays together
    int k = start;
    i = start;
    j = end;
    while (k <= end) {
        if (temp[i] <= temp[j])
            orig[k++] = temp[i++];
        else {
            orig[k++] = temp[j--];
            // increment inversions counter
            count += middle - i + 1;
        }
    }

    free(temp);
    return count;
}

ll merge_sort_count_inv(int orig[], int start, int end) {
    if (start >= end) {
        return 0;
    }
    int middle = (start + end) / 2;

    // recurse on the left and right parts of the array
    // and merge them together
    return merge_sort_count_inv(orig, start, middle) +
           merge_sort_count_inv(orig, middle + 1, end) +
           merge(orig, start, middle, end);
}

int main() {
    FILE* input = fopen("Inverse.in", "r");

    int n;
    fscanf(input, "%d", &n);

    int* arr = malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++) {
        fscanf(input, "%d", &arr[i]);
    }
    fclose(input);

    FILE* output = fopen("Inverse.out", "w+");
    fprintf(output, "%lld", merge_sort_count_inv(arr, 0, n - 1));
    fclose(output);
    
    free(input);

    return 0;
}