#include <iostream>
#include <math.h>
#include <tuple>
#include <vector>

template <typename T> struct Node {
    Node* left;
    Node* right;
    Node* parent;

    T key;
    // subtree sum (including this element)
    T sum_;

    Node(const T& key)
        : key(key), sum_(key), left(nullptr), right(nullptr), parent(nullptr){};
    Node(const T& key, Node<T>* left, Node<T>* right)
        : key(key), sum_(key), left(left), right(right), parent(nullptr){};

    // convinience method for rotating
    // sets children's parent pointer to this
    void attach_children() {
        if (left)
            left->parent = this;
        if (right)
            right->parent = this;

        this->sum_ = sum(left) + sum(right) + key;
    }
};

template <typename T> T sum(Node<T>* root) { return (root) ? root->sum_ : 0; }

template <typename T> class SplayTree {
    Node<T>* root;

    static Node<T>* rotate(Node<T>* parent, Node<T>* child);
    static Node<T>* splay(Node<T>* node);
    static Node<T>* merge(Node<T>* left, Node<T>* right);
    static Node<T>* closest(Node<T>* root, T key);

    template <typename Comp = std::less_equal<T>>
    std::pair<Node<T>*, Node<T>*> split(Node<T>* root, T key) {
        if (!root)
            return {nullptr, nullptr};

        root = closest(root, key);

        if (Comp()(root->key, key)) {
            Node<T>* right = root->right;
            root->right = nullptr;

            if (right) {
                right->parent = nullptr;
                root->sum_ -= sum(right);
            }

            return {root, right};
        } else {
            Node<T>* left = root->left;
            root->left = nullptr;

            if (left) {
                left->parent = nullptr;
                root->sum_ -= sum(left);
            }

            return {left, root};
        }
    }

    static void clear(Node<T>* root) {
        if (!root)
            return;

        clear(root->left);
        clear(root->right);

        delete root;
    }

  public:
    SplayTree() : root(nullptr) {}

    void insert(const T&);
    T subtree_sum(T from, T to);

    ~SplayTree() { clear(root); }
};

template <typename T>
Node<T>* SplayTree<T>::rotate(Node<T>* parent, Node<T>* child) {
    // reattach grandparent if existant
    Node<T>* gparent = parent->parent;
    if (gparent) {
        if (gparent->left == parent)
            gparent->left = child;
        else
            gparent->right = child;
    }
    child->parent = gparent;

    // rotate child and parent
    if (parent->left == child) {
        Node<T>* beta = child->right;
        child->right = parent;
        parent->left = beta;
    } else {
        Node<T>* alpha = child->left;
        child->left = parent;
        parent->right = alpha;
    }

    parent->attach_children();
    child->attach_children();

    return child;
}

template <typename T> Node<T>* SplayTree<T>::splay(Node<T>* node) {
    // id if node already on top
    if (!node->parent)
        return node;

    Node<T>* p = node->parent;
    Node<T>* gp = p->parent;

    if (!gp) {
        rotate(p, node);
        return node;
    }

    if ((gp->left == p) == (p->left == node)) {
        // zig-zig
        rotate(gp, p);
        rotate(p, node);
    } else {
        // zig-zag
        rotate(p, node);
        rotate(gp, node);
    }

    return splay(node);
}

template <typename T>
Node<T>* SplayTree<T>::merge(Node<T>* left, Node<T>* right) {
    if (!right)
        return left;

    if (!left)
        return right;

    right = closest(right, left->key);

    right->left = left;
    right->attach_children();

    return right;
}

// find closest element to given
// returen nullptr of tree is empty
template <typename T> Node<T>* SplayTree<T>::closest(Node<T>* root, T key) {
    if (!root)
        return nullptr;

    if (root->key == key)
        return splay(root);
    if (key < root->key && root->left)
        return closest(root->left, key);
    if (key > root->key && root->right)
        return closest(root->right, key);

    return splay(root);
}

template <typename T> void SplayTree<T>::insert(const T& key) {
    if (!root) {
        root = new Node<T>(key);
        return;
    }

    auto [left, right] = split(root, key);

    if (left && left->key == key) {
        left->right = right;
        left->attach_children();

        root = left;
        return;
    }

    root = new Node<T>(key, left, right);
    root->attach_children();
}

template <typename T> T SplayTree<T>::subtree_sum(T from, T to) {
    auto [less, geq] = split<std::less<T>>(root, from);
    auto [fromto, greater] = split<std::less_equal<T>>(geq, to);

    T _sum = sum(fromto);

    root = merge(less, merge(fromto, greater));

    return _sum;
}

template <typename T> T modsum(T a, T b) { return (a + b) % 1'000'000'000; }

typedef long long ll;

int main() {
    ll _sum = 0;
    SplayTree<ll> tree;

    size_t n;
    std::cin >> n;

    char c;
    while (n--) {
        std::cin >> c;

        switch (c) {
        case '+': {
            ll q;
            std::cin >> q;

            tree.insert(modsum(_sum, q));
            _sum = 0;
            break;
        }

        case '?': {
            ll l, r;
            std::cin >> l >> r;

            _sum = tree.subtree_sum(l, r);
            std::cout << _sum << std::endl;
        }
        }
    }
}

