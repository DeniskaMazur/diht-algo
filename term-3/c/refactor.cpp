#include <iostream>
#include <math.h>
#include <tuple>
#include <vector>

template <typename T>
struct Node {
    Node* left;
    Node* right;
    Node* parent;

    T key;
    size_t size_;

    Node(const T& key) :
        key(key), size_(1), left(nullptr), right(nullptr), parent(nullptr){};
    Node(const T& key, Node<T>* left, Node<T>* right) :
        key(key), size_(1), left(left), right(right), parent(nullptr){};

    // convinience method for rotating
    // sets children's parent pointer to this
    void attach_children() {
        if (left)
            left->parent = this;
        if (right)
            right->parent = this;

        this->size_ = size(left) + size(right) + 1;
    }
};

template <typename T>
T size(Node<T>* root) {
    return (root) ? root->size_ : 0;
}

template <typename T>
class SplayTree {
    Node<T>* root;

    static Node<T>* rotate(Node<T>* parent, Node<T>* child);
    static Node<T>* splay(Node<T>* node);
    static Node<T>* merge(Node<T>* left, Node<T>* right);
    static Node<T>* closest(Node<T>* root, T key);

    template <typename Comp = std::less_equal<T>>
    std::pair<Node<T>*, Node<T>*> split(Node<T>* root, T key) {
        if (!root)
            return {nullptr, nullptr};

        root = closest(root, key);

        if (Comp()(root->key, key)) {
            Node<T>* right = root->right;
            root->right    = nullptr;

            if (right) {
                right->parent = nullptr;
                root->size_ -= size(right);
            }

            return {root, right};
        } else {
            Node<T>* left = root->left;
            root->left    = nullptr;

            if (left) {
                left->parent = nullptr;
                root->size_ -= size(left);
            }

            return {left, root};
        }
    }

    static void clear(Node<T>* root) {
        if (!root)
            return;

        clear(root->left);
        clear(root->right);

        delete root;
    }

    static Node<T>* select_recursive(Node<T>* root, size_t k);

  public:
    SplayTree() : root(nullptr) {}

    void insert(const T&);
    void erase(const T&);

    T select(size_t k);
    size_t size_() const { return size(root); }

    ~SplayTree() { clear(root); }
};

template <typename T>
Node<T>* SplayTree<T>::rotate(Node<T>* parent, Node<T>* child) {
    // reattach grandparent if existant
    Node<T>* gparent = parent->parent;
    if (gparent) {
        if (gparent->left == parent)
            gparent->left = child;
        else
            gparent->right = child;
    }
    child->parent = gparent;

    // rotate child and parent
    if (parent->left == child) {
        Node<T>* beta = child->right;
        child->right  = parent;
        parent->left  = beta;
    } else {
        Node<T>* alpha = child->left;
        child->left    = parent;
        parent->right  = alpha;
    }

    parent->attach_children();
    child->attach_children();

    return child;
}

template <typename T>
Node<T>* SplayTree<T>::splay(Node<T>* node) {
    // id if node already on top
    if (!node->parent)
        return node;

    Node<T>* p  = node->parent;
    Node<T>* gp = p->parent;

    if (!gp) {
        rotate(p, node);
        return node;
    }

    if ((gp->left == p) == (p->left == node)) {
        // zig-zig
        rotate(gp, p);
        rotate(p, node);
    } else {
        // zig-zag
        rotate(p, node);
        rotate(gp, node);
    }

    return splay(node);
}

template <typename T>
Node<T>* SplayTree<T>::merge(Node<T>* left, Node<T>* right) {
    if (!right)
        return left;

    if (!left)
        return right;

    right = closest(right, left->key);

    right->left = left;
    right->attach_children();

    return right;
}

// find closest element to given
// returen nullptr of tree is empty
template <typename T>
Node<T>* SplayTree<T>::closest(Node<T>* root, T key) {
    if (!root)
        return nullptr;

    if (root->key == key)
        return splay(root);
    if (key < root->key && root->left)
        return closest(root->left, key);
    if (key > root->key && root->right)
        return closest(root->right, key);

    return splay(root);
}

template <typename T>
Node<T>* SplayTree<T>::select_recursive(Node<T>* node, size_t k) {
    if (!node)
        return nullptr;

    size_t lsize = size(node->left) + 1;

    if (k == lsize)
        return splay(node);
    else if (k < lsize)
        return select_recursive(node->left, k);
    else
        return select_recursive(node->right, k - lsize);
}

template <typename T>
void SplayTree<T>::insert(const T& key) {
    if (!root) {
        root = new Node<T>(key);
        return;
    }

    auto [left, right] = split(root, key);

    if (left && left->key == key) {
        left->right = right;
        left->attach_children();

        root = left;
        return;
    }

    root = new Node<T>(key, left, right);
    root->attach_children();
}

template <typename T>
void SplayTree<T>::erase(const T& key) {
    if (!root)
        return;

    root           = closest(root, key);
    Node<T>* left  = root->left;
    Node<T>* right = root->right;

    delete root;

    if (left)
        left->parent = nullptr;
    if (right)
        right->parent = nullptr;

    root = merge(left, right);
}

template <typename T>
T SplayTree<T>::select(size_t k) {
   return (root = select_recursive(root, k))->key;
}

typedef long long ll;

int main() {
    SplayTree<ll> tree;

    size_t n;
    std::cin >> n;

    std::string command;
    while (n--) {
        std::cin >> command;

        switch (command[0]) {
        case '1':
        case '+': {
            ll q;
            std::cin >> q;
            tree.insert(q);

            break;
        }

        case '-': {
            ll q;
            std::cin >> q;
            tree.erase(q);
            break;
        }

        case '0': {
            size_t k;
            std::cin >> k;

            std::cout << tree.select(tree.size_() - k + 1) << std::endl;

            break;
        }
        }
    }
}
