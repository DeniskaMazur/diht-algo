#include <bits/stdc++.h>
#include <set>
#include <unordered_map>
#include <iostream>
#include <string>

typedef long long ll;
 
template <typename K, typename V>
class MegaSet {
    private:
        std::unordered_map<K, std::set<V>> data;
        std::unordered_map<V, std::set<K>> val2key;
 
    public:
        void insert(K, V);
        void remove(K, V);
        void clear(K);

        std::set<V> const& get(K);
        std::set<K> const& get_of(V);
};
 
template <typename K, typename V>
void MegaSet<K, V>::insert(K key, V value) {
    data[key].insert(value);
    val2key[value].insert(key);
}
 
template <typename K, typename V>
void MegaSet<K, V>::remove(K key, V value) {
    data[key].erase(value);
    val2key[value].erase(key);
}
 
template <typename K, typename V>
void MegaSet<K, V>::clear(K key) {
    for (auto val: data[key])
        val2key[val].erase(key);
    data[key].clear();
}
 
template <typename K, typename V>
std::set<V> const & MegaSet<K, V>::get(K key) {
    return data[key];
}
 
template <typename K, typename V>
std::set<K> const& MegaSet<K, V>::get_of(V value) {
    return val2key[value];
}
 
template <typename T>
std::ostream& operator<< (std::ostream& os, const std::set<T>& set) {
    if (set.empty()) {
        std::cout << -1;
        return os;
    } 
 
    for (const T& elm: set) {
        os << elm << " ";
    }
    
    return os;
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
 
    MegaSet<ll, ll> ms;
 
    ll q;
    std::cin >> q >> q >> q;

    while (q--) {
       std::string command;
       std::cin >> command;
 
       switch (*command.rbegin()) {
       case 'D': {
            ll e, s;
            std::cin >> e >> s;
            ms.insert(s, e);
            break;
        }
        
        case 'E': {
            ll e, s;
            std::cin >> e >> s;
            ms.remove(s, e);
            break;
        }
        
        case 'R': {
            ll s;
            std::cin >> s;
            ms.clear(s);
            break;
        }
 
        case 'T':
            ll s;
            std::cin >> s;
            std::cout << ms.get(s) << std::endl;
            break;
 
        case 'F': {
            ll e;
            std::cin >> e;
            std::cout << ms.get_of(e) << std::endl;
            break;
        }
        }
    }
}