#include <iostream>
#include <string>

template <typename T>
struct Node {
    T      key;
    size_t height;
    size_t size;

    Node<T>* left;
    Node<T>* right;

    Node(T key) : key(key), height(1), size(0), left(nullptr), right(nullptr) {}
};

template <typename T>
size_t height(Node<T>* node) {
    return node ? node->height : 0;
}

template <typename T>
int diff(Node<T>* node) {
    return height(node->right) - height(node->left);
}

template <typename T>
void calc_height(Node<T>* node) {
    size_t left_h  = height(node->left);
    size_t right_h = height(node->right);

    node->height = std::max(left_h, right_h) + 1;
}

template <typename T>
class AVLTree {
    Node<T>* root;

    static Node<T>* rotate_rr(Node<T>* node);
    static Node<T>* rotate_ll(Node<T>* node);

    static Node<T>* append(Node<T>*, const T&);
    static Node<T>* balance(Node<T>* node);

    static Node<T>* subtree_min(Node<T>*);

    static Node<T>* next_recursive(Node<T>* node, const T& key);
    static Node<T>* prev_recursive(Node<T>* node, const T& key);
    static Node<T>* erase_recursive(Node<T>* node, const T& key);

    static void clear(Node<T>*);

  public:
    AVLTree() : root(nullptr) {}

    void insert(const T& key);
    void erase(const T& key);
    bool exists(const T& key);

    std::pair<T, bool> next(const T& key);
    std::pair<T, bool> prev(const T& key);
    
    ~AVLTree() { clear(root); }
};

template <typename T>
inline void AVLTree<T>::clear(Node<T>* node) {
    if (!node)
        return;

    clear(node->left);
    clear(node->right);

    delete node;
}

template <typename T>
inline Node<T>* AVLTree<T>::rotate_rr(Node<T>* node) {
    Node<T>* tmp = node->left;
    node->left   = tmp->right;
    tmp->right   = node;

    calc_height(node);
    calc_height(tmp);

    return tmp;
}

template <typename T>
inline Node<T>* AVLTree<T>::rotate_ll(Node<T>* node) {
    Node<T>* tmp = node->right;
    node->right  = tmp->left;
    tmp->left    = node;

    calc_height(node);
    calc_height(tmp);

    return tmp;
}

template <typename T>
inline Node<T>* AVLTree<T>::subtree_min(Node<T>* node) {
    if (!node)
        return nullptr;

    while (node->left)
        node = node->left;

    return node;
}

template <typename T>
inline Node<T>* AVLTree<T>::next_recursive(Node<T>* node, const T& key) {
    // find first node that's smaller than given key
    while (node && node->key <= key)
        node = node->right;

    // if none found return null
    if (!node)
        return nullptr;

    // ok. we found a node bigger than given key,
    // but is there a smaller one?
    // let's try and find one in the
    // left subtree
    auto tmp = next_recursive(node->left, key);
    if (tmp)
        node = tmp;

    return node;
}

template <typename T>
inline Node<T>* AVLTree<T>::prev_recursive(Node<T>* node, const T& key) {
    while (node && node->key >= key)
        node = node->left;

    if (!node)
        return nullptr;

    auto tmp = prev_recursive(node->right, key);
    if (tmp)
        node = tmp;

    return node;
}

template <typename T>
inline Node<T>* AVLTree<T>::erase_recursive(Node<T>* node, const T& key) {
    if (!node)
        return nullptr;

    if (key < node->key)
        node->left = erase_recursive(node->left, key);
    else if (key > node->key)
        node->right = erase_recursive(node->right, key);
    else {
        if (!node->right) {
            Node<T>* temp = node->left;

            delete node;
            node = nullptr;

            return temp;
        } else if (!node->left) {
            Node<T>* temp = node->right;

            delete node;
            node = nullptr;

            return temp;
        }

        Node<T>* temp = subtree_min(node->right);
        node->key     = temp->key;
        node->right   = erase_recursive(node->right, temp->key);
    }

    return balance(node);
}

template <typename T>
inline Node<T>* AVLTree<T>::balance(Node<T>* node) {
    calc_height(node);

    if (diff(node) == 2) {
        if (diff(node->right) < 0)
            node->right = rotate_rr(node->right);
        return rotate_ll(node);
    }

    if (diff(node) == -2) {
        if (diff(node->left) > 0)
            node->left = rotate_ll(node->left);
        return rotate_rr(node);
    }

    return node;
}

template <typename T>
inline Node<T>* AVLTree<T>::append(Node<T>* node, const T& key) {
    if (!node)
        return node = new Node<T>(key);

    if (key < node->key)
        node->left = append(node->left, key);
    else if (key > node->key)
        node->right = append(node->right, key);

    return balance(node);
}

template <typename T>
void AVLTree<T>::insert(const T& key) {
    root = append(root, key);
}

template <typename T>
void AVLTree<T>::erase(const T& key) {
    root = erase_recursive(root, key);
}

template <typename T>
bool AVLTree<T>::exists(const T& key) {
    Node<T>* tmp = root;

    while (tmp) {
        if (key == tmp->key)
            return true;
        else if (key < tmp->key)
            tmp = tmp->left;
        else
            tmp = tmp->right;
    }

    return false;
}

template <typename T>
std::pair<T, bool> AVLTree<T>::next(const T& key) {
    auto result = next_recursive(root, key);

    if (!result)
        return std::make_pair(T(), false);

    return std::make_pair(result->key, true);
}

template <typename T>
std::pair<T, bool> AVLTree<T>::prev(const T& key) {
    auto result = prev_recursive(root, key);

    if (!result)
        return std::make_pair(T(), false);

    return std::make_pair(result->key, true);
}

typedef long long ll;

int main() {
    AVLTree<ll> tree;

    std::string input;
    while (std::cin >> input) {
        ll val;
        std::cin >> val;

        switch (input[0]) {
        case 'i':
            tree.insert(val);
            break;

        case 'd':
            tree.erase(val);
            break;

        case 'e':
            std::cout << (tree.exists(val) ? "true" : "false") << std::endl;
            break;

        case 'n': {
            auto [key, found] = tree.next(val);
            if (found)
                std::cout << key << std::endl;
            else
                std::cout << "none" << std::endl;
            break;
        }

        case 'p': {
            auto [key, found] = tree.prev(val);
            if (found)
                std::cout << key << std::endl;
            else
                std::cout << "none" << std::endl;
            break;
        }
        }
    }
}
