#include <cstddef>
#include <iostream>
#include <set>
#include <vector>

class Graph {
    std::vector<std::vector<size_t>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}

    void add_edge(size_t from, size_t to, bool oriented = false) {
        adj[from].push_back(to);

        if (!oriented)
            adj[to].push_back(from);
    }

    const std::vector<size_t>& adj_to(size_t v) const { return adj[v]; }
    size_t size() const { return adj.size(); }
};
 
void dfs(int v,
         int p,
         const Graph& graph,
         std::vector<bool>& used,
         std::vector<int>& d,
         std::vector<int>& up,
         size_t& time,
         std::set<int>& art_points) {
    int children = 0;
    used[v] = true;
    d[v] = up[v] = time++;
 
    for (int to: graph.adj_to(v)) {
        if (to == p)
            continue;
        if (used[to] == 1) {
            up[v] = std::min(up[v], d[to]);
        } else {
            dfs(to, v, graph, used, d, up, time, art_points);
            up[v] = std::min(up[v], up[to]);
            if ((up[to] >= d[v]) && (p != -1))
                art_points.insert(v);
            children++;
        }
    }
    if ((p == -1) && (children != 1))
        art_points.insert(v);
}
 
void find_artpoints(
    const Graph& graph,
    std::set<int>& result
) {
    std::vector<bool> used(graph.size());
    std::vector<int> d(graph.size()), up(graph.size());
    size_t time = 0;
 
    for (int i = 0; i < graph.size(); i++) {
        if (!used[i])
            dfs(i, -1, graph, used, d, up, time, result);
    }
}
 
int main() {
    size_t n, m;
    std::cin >> n >> m;
 
    Graph graph(n);
    for (size_t i = 0; i < m; i++) {
        int u, v;
        std::cin >> u >> v;

        graph.add_edge(u - 1, v - 1, false);
    }
 
    std::set<int> result;
    find_artpoints(graph, result);
    
    std::cout << result.size() << std::endl;
    for (auto v: result) {
        std::cout << v + 1 << " ";
    }
    std::cout << std::endl;
}