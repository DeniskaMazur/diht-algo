#include <cstddef>
#include <iostream>
#include <set>
#include <vector>
 
class Graph {
    std::vector<std::vector<size_t>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}

    void add_edge(size_t from, size_t to, bool oriented = false) {
        adj[from].push_back(to);

        if (!oriented)
            adj[to].push_back(from);
    }

    const std::vector<size_t>& adj_to(size_t v) const { return adj[v]; }
    size_t size() const { return adj.size(); }
};
 
void dfs(size_t u,
         const Graph& graph,
         std::vector<size_t>& tin,
         std::vector<size_t>& tout,
         std::vector<bool>& vis,
         size_t& time) {
    tin[u] = time++;
    vis[u] = true;
 
    for (size_t v: graph.adj_to(u))
        if (!vis[v])
            dfs(v, graph, tin, tout, vis, time);
 
    tout[u] = time++;
}

int main() {
    size_t n;
    std::cin >> n;
 
    Graph graph(n);
    size_t root = 0;
    for (size_t i = 0; i < n; i++) {
        size_t v;
        std::cin >> v;
 
        if (v == 0) {
            root = i;
            continue;
        }
 
        v--;
        graph.add_edge(v, i, true);
    }
 
    std::vector<size_t> tin(n), tout(n, 2 * n - 1);
    std::vector<bool> vis(n);
 
    size_t time = 0;
    dfs(root, graph, tin, tout, vis, time);
 
    size_t m;
    std::cin >> m;
    for (size_t i = 0; i < m; i++) {
        size_t u, v;
        std::cin >> u >> v;
        u--; v--;
 
        std::cout << ((tin[u] < tin[v]) && (tout[u] > tout[v])) << std::endl;
    }
}