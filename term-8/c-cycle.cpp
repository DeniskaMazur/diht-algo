#include <cstddef>
#include <iostream>
#include <utility>
#include <vector>


class Graph {
    std::vector<std::vector<size_t>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}

    void add_edge(size_t from, size_t to, bool oriented = false) {
        adj[from].push_back(to);

        if (!oriented)
            adj[to].push_back(from);
    }

    const std::vector<size_t>& adj_to(size_t v) const { return adj[v]; }
    size_t size() const { return adj.size(); }
};
 
void dfs(size_t v,
         const Graph& graph,
         std::vector<char>& visited,
         std::vector<size_t>& path,
         bool& flag) {
 
    if (flag)
        return;
 
    visited[v] = 1;
    path.push_back(v);
 
    for (size_t i = 0; i < graph.adj_to(v).size(); i++) {
        size_t to = graph.adj_to(v)[i];
        if (visited[to] == 1) {
            path.push_back(to);
            flag = 1;
            return;
        } else {
            dfs(to, graph, visited, path, flag);
        }
 
        if (flag == 1)
            return;
    }
 
    visited[v] = 2;
    path.pop_back();
}

std::pair<std::vector<size_t>, bool> find_cycle(const Graph& graph) {
    const size_t n = graph.size();
    std::vector<size_t> result;

    std::vector<size_t> path;
    std::vector<char> visited(n);
    bool flag = false;
 
    for (size_t i = 0; i < n; i++) {
        if (!visited[i])
            dfs(i, graph, visited, path, flag);
        if (flag)
            break;
    }

    if (!flag)
        return std::make_pair(result, false);

    size_t i = path.size() - 2;
    size_t to = path.back();
 
    while (path[i] != to)
        i--;
    for (; i < path.size() - 1; i++)
        result.push_back(path[i]);

    return std::make_pair(result, true);
}


int main() {
    size_t n, m;
    std::cin >> n >> m;
 
    Graph graph(n);
    for (size_t i = 0; i < m; i++) {
        size_t v, u;
        std::cin >> v >> u;
 
        graph.add_edge(v - 1, u - 1, true);
    }
 
    auto [result, found] = find_cycle(graph);
    if (!found) {
        std::cout << "NO" << std::endl;
        return 0;
    }

    std::cout << "YES" << std::endl;
    for (const size_t& entry: result)
        std::cout << entry + 1 << " ";

    std::cout << std::endl;
}