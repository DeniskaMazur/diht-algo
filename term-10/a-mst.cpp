#include <cstddef>
#include <iostream>
#include <vector>
#include <queue>
 
typedef long long ll;
const ll kInf = 1e18;
 
typedef std::pair<ll, size_t> edge;

class Graph {
    std::vector<std::vector<edge>> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}
 
    void add_edge(size_t from, size_t to, ll dist) { adj[from].push_back(edge(dist, to)); }
    size_t size() const { return adj.size(); }
 
    const std::vector<edge>& adj_to(size_t v) const { return adj[v]; }
};

ll mst_weight(const Graph& graph, size_t src) {
    const size_t n = graph.size();

    std::priority_queue<edge, std::vector<edge>, std::greater<edge>> pq;
    pq.push(std::make_pair(0, src));

    std::vector<bool> added(n);
    size_t total_mst_weight = 0;

    while (!pq.empty()) {
        edge item = pq.top();
        pq.pop();

        size_t weight = item.first;
        size_t ix = item.second;

        if (!added[ix]) {
            total_mst_weight += weight;
            added[ix] = true;
        }

        for(auto& adj_edge: graph.adj_to(ix)) {
            size_t adj_ix = adj_edge.second;
            if (!added[adj_ix])
                pq.push(adj_edge);
        }
    }

    return total_mst_weight;
}

int main() {
    size_t n, m;
    std::cin >> n >> m;

    Graph graph(n);
    for (size_t i = 0; i < m; i++) {
        size_t v, u;
        ll w;

        std::cin >> v >> u >> w;
        graph.add_edge(v - 1, u - 1, w);
        graph.add_edge(u - 1, v - 1, w);
    }

    std::cout << mst_weight(graph, 0);
}