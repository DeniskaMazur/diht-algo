#include <cstddef>
#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
 
typedef long long ll;
const ll kInf = 1e18;
 
// weight, from, to
typedef std::pair<ll, std::pair<size_t, size_t>> edge;

class Graph {
    std::vector<edge> adj;
  public:
    Graph(size_t n_vert): adj(n_vert) {}
 
    void add_edge(size_t from, size_t to, ll dist) { adj.push_back({dist, {from, to}}); }
    size_t size() const { return adj.size(); }
 
    // const std::vector<edge>& adj_to(size_t v) const { return adj[v]; }

    edge& operator[](size_t i) { return adj[i]; }

    void sort_edges_by_weight() {
        std::sort(adj.begin(), adj.end());
    }
};

class KruskalSolver {
    Graph& graph;
    std::vector<edge> mst;
    std::vector<size_t> parents;

    size_t find_set(size_t i) {
        if (i == parents[i])
            return i;

        return parents[i] = find_set(parents[i]);
    }

    void unite_sets(size_t u, size_t v) {
        parents[u] = parents[v];
    }

  public:
    KruskalSolver(Graph& graph): graph(graph), parents(graph.size()) {
        for (size_t i = 0; i < graph.size(); ++i)
            parents[i] = i;
    }

    ll mst_weight() {
        graph.sort_edges_by_weight();

        for (size_t i = 0; i < graph.size(); i++) {
            size_t v = find_set(graph[i].second.first);
            size_t u = find_set(graph[i].second.second);
            if (v != u) {
                unite_sets(v, u);
                mst.push_back(graph[i]);
            }
        }

        ll result = 0;
        for (const edge& e: mst)
            result += e.first;

        return result;
    }
};

int main() {
    size_t n, m;
    std::cin >> n >> m;

    Graph graph(n);
    for (size_t i = 0; i < m; i++) {
        size_t v, u;
        ll w;

        std::cin >> v >> u >> w;
        graph.add_edge(v - 1, u - 1, w);
        graph.add_edge(u - 1, v - 1, w);
    }

    std::cout << KruskalSolver(graph).mst_weight() << std::endl;
}