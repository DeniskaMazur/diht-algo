/*
Дана очень длинная последовательность целых чисел длины N. Требуется вывести в отсортированном виде её наименьшие K элементов.
Последовательность может не помещаться в память. Время работы O(N⋅logK), память O(K). 1≤N≤105, 1≤K≤500
*/

#include "algorithm"
#include "cassert"
#include "iostream"
#include "string"
#include "vector"
 
template <typename T>
struct KV {
    T value;
    size_t key;
};
 
template <typename T, typename Cmp = std::less<T>>
class Heap {
private:
    Cmp comp;
 
    std::vector<T> arr;
 
    void swap(size_t i, size_t j);
 
    void sift_up(size_t i);
    void sift_down(size_t i);
 
public:
    Heap(size_t cap);
    explicit Heap();
 
    T top();
    T extract_top();

    void insert(T elm);
};
 
 
template <typename T, typename Cmp>
Heap<T, Cmp>::Heap(size_t cap) {
    arr.reserve(cap);
}
 
template <typename T, typename Cmp>
Heap<T, Cmp>::Heap() {
    arr.reserve(1000000);
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::swap(size_t i, size_t j) {
        std::swap(arr[i], arr[j]);
 
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::sift_up(size_t i) {
   while (comp(arr[i], arr[(i - 1) / 2]) && i > 0) {
       swap(i, (i - 1) / 2);
       i = (i - 1) / 2;
   }
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::sift_down(size_t i) {
    for (;;) {
        size_t smallest = i;
 
        size_t left = 2 * i + 1;
        size_t right = 2 * i + 2;
    
        if (left < arr.size() && comp(arr[left], arr[smallest])) 
            smallest = left; 
    
        if (right < arr.size() && comp(arr[right], arr[smallest])) 
            smallest = right; 
    
        if (smallest != i) {
            swap(smallest, i);
            i = smallest;
        } else {
            break;
        }
    }
}
 
template <typename T, typename Cmp>
T Heap<T, Cmp>::top() {
    return arr[0];
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::insert(T elm) {
    arr.push_back(elm);
 
    sift_up(arr.size() - 1);
}
 
template <typename T, typename Cmp>
inline T Heap<T, Cmp>::extract_top() {
    T _top = arr[0];
 
    // put last element in place of the first one
    arr[0] = arr.back();
    arr.pop_back();
 
    sift_down(0);
 
    return _top;
}
 
int main() {
    Heap<int, std::greater<int>> heap(100);
 
    int n, k;
    std::cin >> n >> k;
 
    for (ssize_t i = 0; i < k; i++) {
        int p;
        std::cin >> p;
        heap.insert(p);
    }
 
    for (ssize_t i = 0; i < n - k; i++) {
        int p;
        std::cin >> p;
 
        if (p < heap.top()) {
            heap.extract_top();
            heap.insert(p);
        }
    }
 
 
    std::vector<int> inv(k);
    for (ssize_t i = 0; i < k; i++) {
        inv[k - i - 1] = heap.extract_top();
    }
 
    for (ssize_t i = 0; i < k; i++) {
        std::cout << inv[i] << " ";
    }
 
    return 0;
}
 