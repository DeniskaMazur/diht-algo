/*
Реализуйте двоичную кучу.

Обработайте запросы следующих видов:

insert x: вставить целое число x в кучу;
getMin: вывести значение минимального элемента в куче (гарантируется, что к этому моменту куча не пуста);
extractMin: удалить минимальный элемент из кучи (гарантируется, что к этому моменту куча не пуста);
decreaseKey i Δ: уменьшить число, вставленное на i-м запросе, на целое число Δ≥0 (гарантируется, что i-й запрос был осуществлён ранее,
являлся запросом добавления, а добавленное на этом шаге число всё ещё лежит в куче).
Можете считать, что в любой момент времени все числа, хранящиеся в куче, попарно различны, а их количество не превышает 105.
*/

#include "algorithm"
#include "cassert"
#include "iostream"
#include "string.h"
#include "vector"
 
 
template <typename T>
struct KV {
    T value;
    size_t key;

    KV(const T& _value, size_t _key) : value(_value), key(_key) {}
    KV() {};
};
 
template <typename T, typename Cmp = std::less<T>>
class Heap {
private:
    Cmp comp;
 
    std::vector<KV<T>> arr;
    std::vector<size_t> key2pos;
 
    void swap(size_t i, size_t j);
 
    void ensure_capacity();
    
    void sift_up(size_t i);
    void sift_down(size_t i);
 
public:
    Heap(size_t cap);
 
    T top();
    void extract_top();
 
    void insert(T elm, size_t key);
    void decrease_key(size_t key, long long value);
};
 
 
template <typename T, typename Cmp>
Heap<T, Cmp>::Heap(size_t cap) {
    // reserve is a better fit that resize, cuz resize affercts the .size() method,
    // which is used pretty much everywhere in the code
    // also reserve takes up less time anyway
    // as it doesn't call the class constructor
    arr.reserve(cap);
    key2pos.reserve(cap);
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::swap(size_t i, size_t j) {
        std::swap(
           key2pos[arr[i].key],
           key2pos[arr[j].key]
        );
        std::swap(arr[i], arr[j]);
 
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::sift_up(size_t i) {
   while ((arr[i].value < arr[(i - 1) / 2].value) && i > 0) {
       swap(i, (i - 1) / 2);
       i = (i - 1) / 2;
   }
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::sift_down(size_t i) {
    for (;;) {
        size_t smallest = i;
 
        size_t left = 2 * i + 1;
        size_t right = 2 * i + 2;

        if (left < arr.size() && (arr[left].value < arr[smallest].value)) 
            smallest = left; 
    
        if (right < arr.size() && (arr[right].value < arr[smallest].value)) 
            smallest = right; 
    
        if (smallest != i) {
            swap(smallest, i);
            i = smallest;
        } else {
            break;
        }
    }
}
 
template <typename T, typename Cmp>
T Heap<T, Cmp>::top() {
    return arr[0].value;
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::insert(T elm, size_t key) {
    arr.emplace_back(elm, key);
    key2pos[key] = arr.size() - 1;
 
    sift_up(arr.size() - 1);
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::extract_top() {
    arr[0] = arr.back();
    key2pos[arr[0].key] = 0;
    arr.pop_back();
 
    sift_down(0);
}
 
template <typename T, typename Cmp>
inline void Heap<T, Cmp>::decrease_key(size_t key, long long value) {
    arr[key2pos[key]].value -= value;
    sift_up(key2pos[key]);
}
 
typedef long long ll;
 
int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
    std::cout.tie(NULL);
 
    struct less {
        inline bool operator() (const ll& lhs, const ll& rhs) {
            return lhs < rhs;
        }
    };
    Heap<long long, less> heap(1e6);

    size_t n;
    scanf("%ld", &n);

    for (size_t i = 1; i <= n; i++) {
        char s[10];
        scanf("%s", s);

        if (strcmp(s, "insert") == 0) {
            long long x;
            scanf("%lld", &x);
            heap.insert(x, i);
        } else if (strcmp(s, "getMin") == 0) {
            printf("%lld\n", heap.top());
        } else if (strcmp(s, "extractMin") == 0) {
            heap.extract_top();
        } else if (strcmp(s, "decreaseKey") == 0) {
            size_t i;
            long long delta;

            scanf("%ld %lld", &i, &delta);
            heap.decrease_key(i, delta);
        }
    }
}