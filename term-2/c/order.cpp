#include "vector"
#include "iostream"
#include <bits/stdc++.h>

typedef long long ll;

int partition(std::vector<ll>& inputs, int l, int r) {
    int pivot_i = rand() % (r - l + 1) + l;
	ll pivot = inputs[pivot_i];
	std::swap(inputs[pivot_i], inputs[r]);

	int i = l;
	int j = r;

	while(i <= r && inputs[i] <= pivot ) i++;
	if (i >= r) return r;

	j = i + 1;

	while (j < r) {
		while(inputs[j] > pivot) j++;
		if (j < r) std::swap(inputs[i++], inputs[j++]);
	}

	std::swap(inputs[i], inputs[r]);
	return i;
 }

ll quick_select(std::vector<ll>& inputs, int n, int k) {
    int l = 0, r = n - 1;

    for (;;) {
        int pivot_i = partition(inputs, l, r);

        if (pivot_i == k)
            return inputs[pivot_i];
        else if (k < pivot_i)
            r = pivot_i;
        else
            l = pivot_i + 1;
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);

    size_t n, k;
    std::cin >> n >> k;

    std::vector<ll> inputs(n);
    for (int i = 0; i < n; i++) {
        std::cin >> inputs[i];
    }
    
    std::cout << quick_select(inputs, n, k);
}