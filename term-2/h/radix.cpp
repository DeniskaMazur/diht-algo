#include "algorithm"
#include "array"
#include "cassert"
#include "vector"
#include "iostream"
#include "bits/stdc++.h"
 
 
inline unsigned int i_byte(long long x, ssize_t i) {
    assert(i < 8);
    return (x >> (8 * i)) & 0xff;
}
 
 
void radix_sort(std::vector<long long>& a, ssize_t n) {
    std::vector<long long> b(n);
    for (ssize_t k = 0; k < 8; k++) {
        std::array<size_t, 256> c{};
 
        for (ssize_t i = 0; i < n; i++) {
            c[i_byte(a[i], k)]++;
        }
 
        ssize_t count = 0;
        for (ssize_t i = 0; i < 256; i++) {
            ssize_t tmp = c[i];
            c[i] = count;
            count += tmp;
        }
 
        for (ssize_t i = 0; i < n; i++) {
            auto d = i_byte(a[i], k);
            b[c[d]] = a[i];
            c[d]++;
        }
 
        a = b;
    }
}
int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(NULL);
 
    ssize_t n;
    std::cin >> n;
 
    std::vector<long long> a(n);
    for (ssize_t i = 0; i < n; i++) {
        std::cin >> a[i];
    }

    radix_sort(a, n);
 
    for (ssize_t i = 0; i < n; i++) {
         std::cout << a[i] << " "; 
    }
}