#include <algorithm>
#include <climits>
#include <cstddef>
#include <iostream>
#include <utility>
#include <vector>

typedef long long ll;
const ll ALOT = 1e17;

bool is_bit_if(size_t mask, size_t pos) { return ((mask >> pos) & 1); }

std::pair<size_t, std::vector<ll>> calculate_route(const std::vector<std::vector<ll>>& distances) {
    size_t n = distances.size();

    if (n == 1) {
        std::cout << 0 << std::endl << 1 << std::endl;
        return std::make_pair(0, std::vector<ll>(1, 1));
    }

    std::vector<std::vector<ll>> dp(n, std::vector<ll>(1 << n, ALOT));
    for (size_t i = 1; i < n; i++)
        dp[i][1 << i] = 0;

    size_t fullMask = (1 << n) - 1;

    std::vector<std::vector<size_t>> parent(n,
                                            std::vector<size_t>(1 << n, ALOT));

    for (size_t mask = 0; mask < (1 << n); mask++) {
        for (size_t x = 0; x < n; x++) {
            for (size_t y = 0; y < n; y++) {
                if (is_bit_if(mask, y))
                    continue;

                ll newMask = mask | (1 << y);
                if (dp[y][newMask] > dp[x][mask] + distances[x][y]) {
                    parent[y][newMask] = x;
                    dp[y][newMask]     = dp[x][mask] + distances[x][y];
                }
            }
        }
    }

    ll result = ALOT;
    size_t i_max;
    for (size_t x = 0; x < n; x++) {
        if (dp[x][fullMask] < result) {
            i_max  = x;
            result = dp[x][fullMask];
        }
    }

    std::vector<ll> history;
    for (size_t mask = fullMask; mask != 0;) {
        history.push_back(i_max);

        size_t newpos = parent[i_max][mask];
        mask &= (~(1 << i_max));
        i_max = newpos;
    }

    std::reverse(history.begin(), history.end());

    return std::make_pair(result, history);
}

int main() {
    size_t n;
    std::cin >> n;

    if (n == 1) {
        std::cout << 0 << std::endl << 1 << std::endl;
        return 0;
    }

    std::vector<std::vector<ll>> distances(n, std::vector<ll>(n));
    for (auto& row : distances)
        for (auto& e : row)
            std::cin >> e;

    auto [result, history] = calculate_route(distances);

    std::cout << result << std::endl;
    for (const ll& entry: history)
        std::cout << entry + 1 << " ";

    std::cout << std::endl;
}
