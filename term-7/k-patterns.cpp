#include <algorithm>
#include <iostream>
#include <vector>

bool is_bit_of(size_t x, size_t of) { return (x >> of) & 1; }

size_t solve(size_t n, size_t m) {
    std::vector<std::vector<size_t>> dp(m, std::vector<size_t>(1 << n));

    for (size_t i = 0; i < 1 << n; i++) {
        dp[0][i] = 1;
    }

    for (size_t j = 0; j < m - 1; j++) {
        for (size_t mask1 = 0; mask1 < 1 << n; mask1++) {
            for (size_t mask2 = 0; mask2 < 1 << n; mask2++) {

                bool allgood = true;
                for (size_t r = 1; r < n; r++) {
                    if (is_bit_of(mask1, r - 1) == is_bit_of(mask2, r - 1) &&
                        is_bit_of(mask1, r) == is_bit_of(mask2, r) &&
                        is_bit_of(mask1, r) == is_bit_of(mask1, r - 1)) {
                        allgood = false;
                        break;
                    }
                }

                if (allgood)
                    dp[j + 1][mask2] += dp[j][mask1];
            }
        }
    }

    size_t result = 0;
    for (size_t i = 0; i < 1 << n; i++)
        result += dp[m - 1][i];

    return result;
}

int main() {
    size_t m, n;
    std::cin >> m >> n;

    if (n > m)
        std::swap(n, m);

    
    std::cout << solve(n, m);
}

