#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
 
class BigInteger {
    friend std::ostream& operator<<(std::ostream&, const BigInteger&);
    friend std::istream& operator>>(std::istream&, BigInteger&);
 
    friend bool operator<(const BigInteger& lhs, const BigInteger& rhs);
 
    std::vector<int> backing;
    bool negative;
 
    bool is_zero() const { return backing.size() == 1 && backing[0] == 0; }
    void pop_zeroes();
 
  public:
    static const int BASE = 10000;
    static const int BASE_LOG_10 = 4;
 
    BigInteger() : negative(false) { backing.push_back(0); }
    BigInteger(int);
    explicit BigInteger(const std::string&);
 
    BigInteger operator-() const;
 
    BigInteger& operator+=(const BigInteger& rhs);
    BigInteger& operator-=(const BigInteger& rhs);
 
    BigInteger& operator*=(const BigInteger& rhs);
    BigInteger& operator/=(const BigInteger& rhs);
    BigInteger& operator%=(const BigInteger& rhs);
 
    BigInteger& operator++() { return *this += 1; }
    BigInteger& operator--() { return *this -= 1; }
 
    BigInteger operator++(int) {
        BigInteger temp = *this;
        ++*this;
        return temp;
    }
    BigInteger operator--(int) {
        BigInteger temp = *this;
        ++*this;
        return temp;
    }
 
    std::string toString() const;
    void flip_sign() { negative = !negative; }
    bool is_negative() const { return negative; }
 
    explicit operator bool() const { return !is_zero(); }
 
    BigInteger abs() const {
        if (*this < 0)
            return -(*this);
        return *this;
    }
};
 
bool operator<(const BigInteger& lhs, const BigInteger& rhs) {
    if (lhs.negative != rhs.negative) {
        return lhs.negative;
    }
    if (rhs.backing.size() != lhs.backing.size()) {
        if (lhs.negative)
            return lhs.backing.size() > rhs.backing.size();
        return lhs.backing.size() < rhs.backing.size();
    }
    for (int i = lhs.backing.size() - 1; i >= 0; --i) {
        if (lhs.backing[i] != rhs.backing[i]) {
            if (lhs.negative)
                return lhs.backing[i] > rhs.backing[i];
            return lhs.backing[i] < rhs.backing[i];
        }
    }
    return false;
}
 
bool operator>(const BigInteger& lhs, const BigInteger& rhs) {
    return rhs < lhs;
}
 
bool operator==(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}
bool operator!=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs == rhs);
}
 
bool operator<=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs > rhs);
}
bool operator>=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs < rhs);
}
 
BigInteger::BigInteger(const std::string& sss) : negative(false) {
    std::string str = sss;
    if (str.empty()) {
        backing.push_back(0);
        return;
    }
 
    if (str[0] == '-') {
        negative = true;
        str[0] = '0';
    }
 
    for (int i = str.size(); i > 0;) {
        size_t i_next = std::max(i - BASE_LOG_10, 0);
 
        backing.push_back(std::stoi(str.substr(i_next, i - i_next)));
 
        i = i_next;
    }
 
    pop_zeroes();
}
 
BigInteger::BigInteger(int val) : negative(val < 0) {
    if (negative)
        val *= -1;
 
    while (val > 0) {
        backing.push_back(val % BASE);
        val /= BASE;
    }
 
    if (backing.size() == 0)
        backing.push_back(0);
}
 
void BigInteger::pop_zeroes() {
    while (backing.size() > 1 && backing.back() == 0) {
        backing.pop_back();
    }
}
 
std::ostream& operator<<(std::ostream& os, const BigInteger& rhs) {
    if (rhs.is_zero())
        return os << 0;
 
    if (rhs.negative)
        os << "-";
 
    std::vector<int>::const_reverse_iterator iter = rhs.backing.rbegin();
 
    if (*iter)
        os << *iter;
 
    iter++;
 
    for (; iter != rhs.backing.rend(); iter++) {
        os.width(BigInteger::BASE_LOG_10);
        os.fill('0');
        os << *iter;
    }
    return os;
}
 
std::istream& operator>>(std::istream& is, BigInteger& rhs) {
    std::string str;
    is >> str;
 
    BigInteger n(str);
    rhs = n;
 
    return is;
}
 
std::string BigInteger::toString() const {
    std::string str;
 
    if (is_zero())
        return str = "0";
 
    if (negative)
        str += '-';
 
    std::vector<int>::const_reverse_iterator iter = backing.rbegin();
    if (*iter)
        str += std::to_string(*iter);
 
    iter++;
 
    for (; iter != backing.rend(); iter++) {
        std::string tmp = std::to_string(*iter);
        str += std::string(BASE_LOG_10, '0')
                   .replace(BASE_LOG_10 - tmp.size(), tmp.size(), tmp);
    }
 
    return str;
}
 
BigInteger BigInteger::operator-() const {
    BigInteger x = *this;
    if (x.is_zero())
        return x;
 
    x.negative = !x.negative;
    return x;
}
 
BigInteger& BigInteger::operator+=(const BigInteger& rhs) {
    if (negative != rhs.negative) {
        negative = !negative;
        *this -= rhs;
        negative = !negative;
 
        pop_zeroes();
        return *this;
    }
 
    size_t _size = std::max(backing.size() + 1, rhs.backing.size() + 1);
    backing.resize(_size, 0);
 
    int more = 0;
    for (size_t i = rhs.backing.size() - 1; i < rhs.backing.size(); --i) {
        backing[i] += rhs.backing[i] + more;
 
        more = backing[i] / BASE;
        backing[i] -= more * BASE;
 
        for (int j = 1; more; ++j) {
            backing[i + j] += more;
            more = backing[i + j] / BASE;
            backing[i + j] -= more * BASE;
        }
    }
 
    pop_zeroes();
    return *this;
}
 
BigInteger operator+(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result += rhs;
}
 
BigInteger& BigInteger::operator-=(const BigInteger& rhs) {
    if (*this == rhs)
        return *this = 0;
 
    if (negative != rhs.negative) {
        negative = !negative;
        *this += rhs;
        negative = !negative;
 
        return *this;
    }
 
    const BigInteger* bigger_ptr = &rhs;
    const BigInteger* smaller_ptr = &rhs;
 
    if (!negative) {
        if (rhs > *this) {
            bigger_ptr = this;
            negative = true;
        } else {
            smaller_ptr = this;
        }
    } else {
        if (rhs < *this) {
            bigger_ptr = this;
            negative = false;
        } else {
            smaller_ptr = this;
        }
    }
 
    while (backing.size() < rhs.backing.size())
        backing.push_back(0);
 
    int per = 0;
    for (size_t i = 0; i < bigger_ptr->backing.size() || per == 1; ++i) {
        backing[i] =
            (i < bigger_ptr->backing.size())
                ? smaller_ptr->backing[i] - (bigger_ptr->backing[i] + per)
                : smaller_ptr->backing[i] - (per);
        per = 0;
        if (backing[i] < 0) {
            backing[i] += BASE;
            per = 1;
        }
    }
 
    pop_zeroes();
    return *this;
}
 
BigInteger operator-(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result -= rhs;
}
 
BigInteger& BigInteger::operator*=(const BigInteger& rhs) {
    BigInteger temp(0);
 
    if (rhs.is_zero() || (*this).is_zero())
        return *this = temp;
 
    temp.backing.resize(backing.size() + rhs.backing.size() + 1, 0);
 
    for (size_t i = backing.size() - 1; i <= backing.size(); --i) {
        for (size_t j = rhs.backing.size() - 1; j <= rhs.backing.size(); --j) {
            temp.backing[i + j] += (backing[i] * rhs.backing[j]);
            int per = temp.backing[i + j] / BASE;
 
            temp.backing[i + j] %= BASE;
 
            ssize_t per_index = i + j + 1;
 
            while (per > 0) {
                temp.backing[per_index] += per % BASE;
                per = temp.backing[per_index] / BASE;
                temp.backing[per_index] %= BASE;
                ++per_index;
            }
        }
    }
 
    //                    a.k.a xor
    temp.negative = negative != rhs.negative;
    temp.pop_zeroes();
 
    return *this = temp;
}
 
BigInteger operator*(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result *= rhs;
}
 
BigInteger& BigInteger::operator/=(const BigInteger& rhs) {
    BigInteger answer = 0;
    if (!rhs || !(*this))
        return *this = 0;
 
    BigInteger tmp = rhs;
    tmp.negative = false;
 
    BigInteger helper2 = tmp;
    BigInteger helper1 = (*this);
 
    helper1.negative = false;
 
    BigInteger prev_s, prev_d, degree;
    while (helper1 >= helper2) {
        degree = 1;
 
        prev_s = helper2;
        prev_d = degree;
 
        while (helper1 >= helper2) {
            prev_s = helper2;
            prev_d = degree;
 
            helper2 *= BASE;
            degree *= BASE;
        }
 
        helper2 = prev_s;
        degree = prev_d;
 
        while (helper1 >= helper2) {
            helper1 -= helper2;
            answer += degree;
        }
 
        helper2 = tmp;
        helper2.pop_zeroes();
    }
 
    answer.negative = (negative != rhs.negative);
    answer.pop_zeroes();
 
    return (*this) = answer;
}
 
BigInteger operator/(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result /= rhs;
}
 
BigInteger& BigInteger::operator%=(const BigInteger& rhs) {
    return *this -= rhs * ((*this) / rhs);
}
 
BigInteger operator%(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result %= rhs;
}
 
typedef long long ll;
const ll MOD = 1e9 + 7;
 
template <typename T>
class Matrix {
    size_t dim1, dim2;
    std::vector<std::vector<T>> backing;
 
  public:
    T mod;
 
    Matrix(size_t dim1, size_t dim2, T _mod) :
        dim1(dim1), dim2(dim2), mod(_mod) {
        backing = std::vector<std::vector<T>>(dim1, std::vector<T>(dim2));
    }
 
    T& operator()(size_t i, size_t j) { return backing[i][j]; }
    const T& operator()(size_t i, size_t j) const { return backing[i][j]; }
 
    size_t nrows() const { return dim1; }
    size_t ncols() const { return dim2; }
};
 
template <typename T>
Matrix<T> operator*(const Matrix<T>& lhs, const Matrix<T>& rhs) {
    Matrix<T> result(lhs.nrows(), rhs.ncols(), lhs.mod);
 
    for (size_t i = 0; i < lhs.nrows(); i++) {
        for (size_t j = 0; j < rhs.ncols(); j++) {
            for (size_t k = 0; k < lhs.ncols(); k++) {
                result(i, j) += lhs(i, k) * rhs(k, j);
                result(i, j) %= lhs.mod;
            }
        }
    }
 
    return result;
}
 
template <typename T>
T pow(T base, BigInteger exp) {
    if (exp == 1)
        return base;
 
    T half = pow(base, exp / 2);
    if (exp % 2 == 0)
        return half * half;
    else
        return half * half * base;
}
 
bool is_bit_of(unsigned int mask, unsigned int pos) {
    return (mask >> pos) & 1u;
}
 
bool mask_check(int mask1, int mask2, int i) {
    return is_bit_of(mask1, i) == is_bit_of(mask2, i) &&
            is_bit_of(mask1, i - 1) == is_bit_of(mask2, i - 1) &&
            is_bit_of(mask1, i) == is_bit_of(mask1, i - 1);
}
 
bool verify(int mask1, int mask2, int n) {
    for (int i = 1; i < n; ++i) {
        if (mask_check(mask1, mask2, i)) {
            return false;
        }
    }
    return true;
}
 
int main() {
    int m, mod;
    BigInteger n;
    std::cin >> n >> m >> mod;
    Matrix<int> matr(1 << m, 1 << m, mod);
 
    if (n > 1) {
        for (size_t mask1 = 0; mask1 < 1 << m; mask1++) {
            for (size_t mask2 = 0; mask2 < 1 << m; mask2++) {
                matr(mask1, mask2) = verify(mask1, mask2, m);
            }
        }
 
        matr = pow(matr, n - 1);
    } else {
        for (size_t i = 0; i < 1 << m; ++i)
            matr(i, i) = 1;
    }
 
    Matrix<int> base(1 << m, 1, mod);
    for (size_t i = 0; i < 1 << m; i++)
        base(i, 0) = 1;
 
    matr = matr * base;
    size_t sum = 0;
    for (size_t i = 0; i < 1 << m; i++)
        sum += matr(i, 0);
 
    std::cout << sum % mod << std::endl;
}
