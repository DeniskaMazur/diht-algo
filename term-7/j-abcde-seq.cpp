#include <array>
#include <iostream>
#include <vector>
 
typedef long long ull;
const ull MOD = 999999937;
 
template <typename T>
class Matrix {
    size_t dim1, dim2;
    std::vector<std::vector<T>> backing;
 
  public:
    Matrix(size_t dim1, size_t dim2, T init) : dim1(dim1), dim2(dim2) {
        backing = std::vector<std::vector<T>>(dim1, std::vector<T>(dim2, init));
    }
 
    Matrix(size_t dim1, size_t dim2) : dim1(dim1), dim2(dim2) {
        backing = std::vector<std::vector<T>>(dim1, std::vector<T>(dim2));
    }
 
    T& operator()(size_t i, size_t j) { return backing[i][j]; }
    const T& operator()(size_t i, size_t j) const { return backing[i][j]; }
 
    size_t nrows() const { return dim1; }
    size_t ncols() const { return dim2; }
 
    T sum() {
        T result(0);
        for (size_t i = 0; i < dim1; i++) {
            for (size_t j = 0; j < dim2; j++) {
                result += backing[i][j];
                result %= MOD;
            }
        }
 
        return result;
    }
 
    void print() {
        for (size_t i = 0; i < dim1; i++) {
            for (size_t j = 0; j < dim2; j++) {
                std::cout << backing[i][j] << " ";
            }
            std::cout << std::endl;
        }
    }
};
 
template <typename T>
Matrix<T> operator*(const Matrix<T>& lhs, const Matrix<T>& rhs) {
    Matrix<T> result(lhs.nrows(), rhs.ncols(), 0);
 
    for (size_t i = 0; i < lhs.nrows(); i++) {
        for (size_t j = 0; j < rhs.ncols(); j++) {
            for (size_t k = 0; k < lhs.ncols(); k++) {
                result(i, j) += lhs(i, k) * rhs(k, j);
                result(i, j) %= MOD;
            }
        }
    }
 
    return result;
}
 
template <typename T>
T pow(T base, size_t exp) {
    if (exp == 1)
        return base;
 
    T half = pow(base, exp / 2);
    if (exp % 2 == 0)
        return half * half;
    else
        return half * half * base;
}

int main() {
    Matrix<ull> base(5, 5, 1);
    Matrix<ull> ones(5, 1, 1);
 
    base(2, 3) = 0;
    base(2, 4) = 0;
    base(4, 3) = 0;
    base(4, 4) = 0;
 
    ull exp;
    while (std::cin >> exp) {
        if (exp == 0) {
            return 0;
        }
 
        if (exp == 1) {
            std::cout << 5 << std::endl;
            continue;
        } else {
            std::cout << (pow(base, exp - 1) * ones).sum() % MOD << std::endl;
        }
    }
}
