#include <cmath>
#include <cstddef>
#include <iterator>
#include <utility>
#include <vector>

namespace umap_tooling {

template <typename T, typename Allocator, bool is_const = false>
class chain_iterator;

template <typename T, typename Allocator = std::allocator<T>>
class Chain {
   public:
    struct Node {
        Node* prev;
        Node* next;

        T item;

        Node(Node* prev = nullptr, Node* next = nullptr)
            : prev(prev), next(next) {}

        explicit Node(const T& val, Node* prev = nullptr, Node* next = nullptr)
            : prev(prev), next(next), item(val) {}

        template <typename... Args>
        Node(Args&&... args)
            : prev(nullptr), next(nullptr), item(std::forward<Args>(args)...) {}
    };

    typedef
        typename std::allocator_traits<Allocator>::template rebind_alloc<Node>
            NodeAllocator;

    NodeAllocator alloc;

   private:
    Node* fake_node;
    size_t sz = 0;

    void wire_nodes(Node* new_node, Node* curr_node) {
        new_node->prev = curr_node->prev;
        new_node->next = curr_node;
        curr_node->prev = new_node;
        new_node->prev->next = new_node;
    }

   public:
    typedef chain_iterator<T, Allocator, false> iterator;
    typedef chain_iterator<T, Allocator, true> const_iterator;

    iterator begin() { return iterator(fake_node->next); }
    const_iterator begin() const { return const_iterator(fake_node->next); }
    const_iterator cbegin() const { return begin(); }

    typedef std::allocator_traits<NodeAllocator> AllocTraits;

    iterator end() { return iterator(fake_node); }
    const_iterator end() const { return const_iterator(fake_node); }
    const_iterator cend() const { return end(); }


    explicit Chain()
        : alloc(NodeAllocator()),
          fake_node(reinterpret_cast<Node*>(AllocTraits::allocate(alloc, 1))),
          sz(0) {
        fake_node->next = fake_node->prev = fake_node;
    }

    Chain(const Chain& chn)
        : alloc(AllocTraits::select_on_container_copy_construction(chn.alloc)),
          fake_node(reinterpret_cast<Node*>(AllocTraits::allocate(alloc, 1))),
          sz(0) {
        fake_node->prev = fake_node;
        fake_node->next = fake_node->prev;
        for (const T& i : chn) push_back(i);
    }

    Chain(Chain&& chn)
        : alloc(std::move(chn.alloc)), fake_node(chn.fake_node), sz(chn.sz) {
        chn.clean();
    }

    Chain& operator=(const Chain& chn);
    Chain& operator=(Chain&& chn);

    ~Chain() {
        while (size() > 0) pop_back();
    }

    void clear_node(Node* node) {
        AllocTraits::destroy(alloc, node);
        AllocTraits::deallocate(alloc, node, 1);
    }

    void clean() {
        alloc = Allocator();
        fake_node = reinterpret_cast<Node*>(AllocTraits::allocate(alloc, 1));
        fake_node->next = fake_node->prev = fake_node;

        sz = 0;
    }

    void pop_back();
    void push_back(const T& val);
    void push_front(const T& val);
    void pop_front();

    template <bool is_const>
    void insert(chain_iterator<T, Allocator, is_const> it, T&& val) {
        insert<>(it, val);
    }

    template <bool is_const>
    void erase(chain_iterator<T, Allocator, is_const> it) {
        --sz;

        Node* curr_node = it.get_node();

        curr_node->next->prev = curr_node->prev;
        curr_node->prev->next = curr_node->next;

        AllocTraits::destroy(alloc, curr_node);
        AllocTraits::deallocate(alloc, curr_node, 1);
    }

    template <bool is_const>
    void insert(chain_iterator<T, Allocator, is_const> it, const T& val) {
        insert<>(it, val);
    }

    template <bool is_const, typename... Args>
    void emplace(chain_iterator<T, Allocator, is_const> it, Args&&... args) {
        ++sz;

        Node* curr_node = it.get_node();
        Node* new_node = AllocTraits::allocate(alloc, 1);
        AllocTraits::construct(alloc, new_node, std::forward<Args>(args)...);

        wire_nodes(new_node, curr_node);
    }

    template <bool is_const, typename Value>
    void insert(chain_iterator<T, Allocator, is_const> it, Value&& val) {
        ++sz;

        Node* curr_node = it.get_node();
        Node* new_node = AllocTraits::allocate(alloc, 1);
        AllocTraits::construct(alloc, new_node, std::forward<Value>(val));

        wire_nodes(new_node, curr_node);
    }

    size_t size() const { return sz; }

    friend iterator;
    friend const_iterator;
};

template <typename T, typename Allocator>
Chain<T, Allocator>& Chain<T, Allocator>::operator=(
    const Chain<T, Allocator>& chn) {
    while (size() > 0) pop_back();

    if (AllocTraits::propagate_on_container_copy_assignment::value)
        alloc = chn.alloc;

    for (const T& i : chn) push_back(i);

    return *this;
}

template <typename T, typename Allocator>
Chain<T, Allocator>& Chain<T, Allocator>::operator=(Chain<T, Allocator>&& chn) {
    if (fake_node == chn.fake_node) return *this;

    while (size() > 0) pop_back();

    if (AllocTraits::propagate_on_container_move_assignment::value)
        alloc = chn.alloc;

    fake_node = chn.fake_node;
    sz = chn.sz;

    chn.clean();
    return *this;
}

template <typename T, typename Allocator>
void Chain<T, Allocator>::pop_back() {
    --sz;

    if (fake_node->prev == fake_node->next) {
        clear_node(fake_node->prev);
        fake_node->prev = fake_node->next = fake_node;
        return;
    }

    Node* last_node = fake_node->prev->prev;
    clear_node(fake_node->prev);

    last_node->next = fake_node;
    fake_node->prev = last_node;
}

template <typename T, typename Allocator>
void Chain<T, Allocator>::push_back(const T& val) {
    ++sz;

    if (fake_node->prev == fake_node) {
        auto new_node = AllocTraits::allocate(alloc, 1);
        AllocTraits::construct(alloc, new_node, val, fake_node, fake_node);

        fake_node->next = fake_node->prev = new_node;
        return;
    }

    Node& last_node = *fake_node->prev;
    Node* new_node = AllocTraits::allocate(alloc, 1);
    AllocTraits::construct(alloc, new_node, val, fake_node->prev, fake_node);

    last_node.next = fake_node->prev = new_node;
}

template <typename T, typename Allocator>
void Chain<T, Allocator>::push_front(const T& val) {
    ++sz;

    Node* fst_node = fake_node->next;
    Node* new_node = AllocTraits::allocate(alloc, 1);
    AllocTraits::construct(alloc, new_node, val, fake_node, fst_node);

    fst_node->prev = new_node;
    fake_node->next = new_node;
}

template <typename T, typename Allocator>
void Chain<T, Allocator>::pop_front() {
    --sz;

    Node* firstNode = fake_node->next->next;
    clear_node(fake_node->next);
    firstNode->prev = fake_node;
    fake_node->next = firstNode;
}

template <typename T, typename Allocator, bool is_const>
class chain_iterator {
    friend Chain<T, Allocator>;

    typedef typename Chain<T, Allocator>::Node NodeType;

    typedef chain_iterator<T, Allocator, false> iterator;
    typedef chain_iterator<T, Allocator, true> const_iterator;

    NodeType* p = nullptr;

   public:
    typedef std::bidirectional_iterator_tag iterator_category;

    typedef T value_type;
    typedef std::ptrdiff_t difference_type;
    typedef std::conditional_t<is_const, const T*, T*> pointer;
    typedef typename std::conditional_t<is_const, const T&, T&> reference;

    chain_iterator() = default;

    explicit chain_iterator(NodeType* pt) { p = pt; }
    chain_iterator(iterator const& it) { p = it.p; }

    explicit operator const_iterator() const { return const_iterator(p); }

    reference operator*() const { return p->item; }
    pointer operator->() const { return &p->item; }

    NodeType* get_node() { return p; }
    const NodeType* get_node() const { return p; }

    chain_iterator<T, Allocator, is_const>& operator=(
        chain_iterator<T, Allocator, is_const> const& other);

    chain_iterator<T, Allocator, is_const>& operator++();
    chain_iterator<T, Allocator, is_const> operator++(int);

    chain_iterator<T, Allocator, is_const>& operator--();
    chain_iterator<T, Allocator, is_const> operator--(int);

    bool operator==(const chain_iterator& other) const { return p == other.p; }
    bool operator!=(const chain_iterator& other) const {
        return !operator==(other);
    }
};

template <typename T, typename Allocator, bool is_const>
chain_iterator<T, Allocator, is_const>&
chain_iterator<T, Allocator, is_const>::operator=(
    chain_iterator<T, Allocator, is_const> const& other) {
    p = other.p;
    return *this;
}

template <typename T, typename Allocator, bool is_const>
chain_iterator<T, Allocator, is_const>&
chain_iterator<T, Allocator, is_const>::operator++() {
    if (p->next != nullptr) p = p->next;
    return *this;
}

template <typename T, typename Allocator, bool is_const>
chain_iterator<T, Allocator, is_const>
chain_iterator<T, Allocator, is_const>::operator++(int) {
    auto old = *this;
    ++*this;
    return old;
}

template <typename T, typename Allocator, bool is_const>
chain_iterator<T, Allocator, is_const>&
chain_iterator<T, Allocator, is_const>::operator--() {
    if (p->prev != nullptr) p = p->prev;
    return *this;
}

template <typename T, typename Allocator, bool is_const>
chain_iterator<T, Allocator, is_const>
chain_iterator<T, Allocator, is_const>::operator--(int) {
    auto old = *this;
    --*this;
    return old;
}
}  // namespace umap_tooling

template <typename Key, class Value, typename Hash = std::hash<Key>,
          typename Equal = std::equal_to<Key>,
          typename Allocator = std::allocator<std::pair<const Key, Value>>,
          size_t hashmap_size = 1024>
class UnorderedMap {
   public:
    typedef std::pair<const Key, Value> NodeType;
    typedef
        typename umap_tooling::Chain<NodeType, Allocator>::iterator Iterator;
    typedef typename umap_tooling::Chain<NodeType, Allocator>::const_iterator
        ConstIterator;

    Iterator begin() { return backing.begin(); }
    Iterator end() { return backing.end(); }

   private:
    typedef std::pair<Iterator, bool> OptionalReturn;

    umap_tooling::Chain<Iterator, Allocator>& get_bucket(const Key& key) {
        return hashmap[Hash()(key) % maxsize()];
    }
    const umap_tooling::Chain<Iterator, Allocator>& get_bucket(
        const Key& key) const {
        return hashmap[Hash()(key) % maxsize()];
    }

    size_t sz = 0;
    double max_load = 1;

    Allocator alloc;

    umap_tooling::Chain<NodeType, Allocator> backing;
    std::vector<umap_tooling::Chain<Iterator, Allocator>> hashmap;

    void rehash(size_t count) {
        if (count <= maxsize()) return;

        hashmap.clear();
        hashmap.resize(count, umap_tooling::Chain<Iterator, Allocator>());

        for (auto it = begin(); it != end(); ++it)
            get_bucket(it->first).push_back(it);
    }

    void destroy_node(NodeType* inserted_node) {
        std::allocator_traits<Allocator>::destroy(alloc, inserted_node);
        std::allocator_traits<Allocator>::deallocate(alloc, inserted_node, 1);
    }

   public:
    ConstIterator cbegin() const { return begin(); }
    ConstIterator cend() const { return end(); }

    size_t size() const { return sz; }
    double max_load_factor() const { return max_load; }
    size_t maxsize() const {
        return std::round(max_load_factor() * hashmap.size());
    }

    UnorderedMap() : alloc(Allocator()) {
        hashmap.resize(hashmap_size,
                       umap_tooling::Chain<Iterator, Allocator>());
    }

    UnorderedMap(const UnorderedMap& map) = default;
    UnorderedMap(UnorderedMap&& map) = default;

    Iterator find(const Key& key) {
        auto& bucket = get_bucket(key);

        for (auto it = bucket.begin(); it != bucket.end(); it++)
            if (Equal()((*it)->first, key)) return *it;

        return end();
    }

    ConstIterator begin() const { return backing.cbegin(); }
    ConstIterator end() const { return backing.cend(); }

    Value& operator[](const Key& key);

    Value& at(const Key& key) {
        auto it = find(key);
        if (it != end()) return it->second;

        throw std::out_of_range("Key not found");
    }

    const Value& at(const Key& key) const { return at(key); }

    template <typename... Args>
    std::pair<Iterator, bool> emplace(Args&&... args);

    template <typename Pair>
    OptionalReturn insert(Pair&& node);

    OptionalReturn insert(NodeType&& node) { return insert<>(node); }
    OptionalReturn insert(const NodeType& node) { return insert<>(node); }

    template <typename InputIterator>
    void insert(InputIterator begin, InputIterator end);

    void erase(Iterator it);
    void erase(Iterator begin, Iterator end);
    void reserve(size_t count) {
        rehash(std::ceil(static_cast<double>(count) / max_load_factor()));
    }

    UnorderedMap& operator=(const UnorderedMap& map) = default;
    UnorderedMap& operator=(UnorderedMap&& map) = default;
};

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
template <typename Pair>
typename UnorderedMap<Key, Value, Hash, Equal, Allocator,
                      hashmap_size>::OptionalReturn
UnorderedMap<Key, Value, Hash, Equal, Allocator, hashmap_size>::insert(
    Pair&& node) {
    auto& bucket = get_bucket(node.first);

    if (find(node.first) != end()) return {find(node.first), false};

    backing.emplace(begin(), std::move(node));
    bucket.emplace(bucket.begin(), begin());

    ++sz;

    auto it = begin();
    if (size() <= maxsize()) return {it, true};

    rehash(1 + maxsize() * 2);
    it = find(node.first);

    return {it, true};
}

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
void UnorderedMap<Key, Value, Hash, Equal, Allocator, hashmap_size>::erase(
    Iterator begin, Iterator end) {
    for (auto it = begin; it != end;) {
        auto next = it;
        ++next;

        erase(it);

        it = next;
    }
}

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
void UnorderedMap<Key, Value, Hash, Equal, Allocator, hashmap_size>::erase(
    Iterator it) {
    auto& key = it->first;
    auto& bucket = get_bucket(key);

    auto lit = bucket.begin();
    lit != bucket.end();
    while (lit != bucket.end()) {
        if (Equal()((*lit)->first, key)) {
            bucket.erase(lit);
            break;
        }
        ++lit;
    }

    backing.erase(it);
    --sz;
}

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
template <typename InputIterator>
void UnorderedMap<Key, Value, Hash, Equal, Allocator, hashmap_size>::insert(
    InputIterator begin, InputIterator end) {
    for (auto&& it = begin; it != end; ++it) insert(static_cast<const NodeType&>(*it));
}

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
template <typename... Args>
std::pair<typename UnorderedMap<Key, Value, Hash, Equal, Allocator,
                                hashmap_size>::Iterator,
          bool>
UnorderedMap<Key, Value, Hash, Equal, Allocator, hashmap_size>::emplace(
    Args&&... args) {
    NodeType* nodeToInsert =
        std::allocator_traits<Allocator>::allocate(alloc, 1);
    std::allocator_traits<Allocator>::construct(alloc, nodeToInsert,
                                                std::forward<Args>(args)...);

    auto res = insert(std::move(*nodeToInsert));

    destroy_node(nodeToInsert);

    return res;
}

template <typename Key, typename Value, typename Hash, typename Equal,
          typename Allocator, size_t hashmap_size>
Value& UnorderedMap<Key, Value, Hash, Equal, Allocator,
                    hashmap_size>::operator[](const Key& key) {
    if (find(key) != end()) {
        if (size() > maxsize()) rehash(maxsize() * 2 + 1);

        return find(key)->second;
    }

    auto& bucket = get_bucket(key);

    backing.push_front({key, Value()});
    bucket.insert(bucket.end(), backing.begin());

    ++sz;

    return find(key)->second;
}
