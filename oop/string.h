#include "cassert"
#include "cstring"
#include "iostream"
#include "sstream"

class String {
    friend std::ostream& operator<<(std::ostream& os, const String& s);
    friend std::istream& operator>>(std::istream& is, String& s);
    friend String        operator+(const String& lhs, const String& rhs);
    friend bool          operator==(const String& lhs, const String& rhs);

  private:
    static const size_t DEFAULT_CAP = 32;
    // we'll keep `backing` null terminated
    // to make it work with c string functions
    char* backing;

    // string size
    // number of chars until \0
    size_t size;
    // size never reaches capacity - 1;
    size_t capacity;

  public:
    // default constructor creates empty string
    String(size_t n = 0);
    // create `String` from c-style string
    String(const char*);
    // create `String` consisting of `n` `c` chars
    explicit String(size_t n, char c);
    // create `String` consisting of one char
    // mostly needed for implicit char type casts
    explicit String(const char);

    String(const String&);
    String& operator=(const String&);

    ~String();

    //
    size_t length() const { return size; }

    void push_back(char);
    void pop_back();

    char&       front() { return *backing; }
    const char& front() const { return *backing; }

    //?
    char&       back() { return *(backing + size - 1); }
    const char& back() const { return *(backing + size - 1); }

    //?
    char& operator[](size_t index) { return backing[index]; }
    char  operator[](size_t index) const { return backing[index]; }

    String& operator+=(const String&);
    String& operator+=(const char);

    bool empty() const { return !size; }
    void clear();

    size_t find(const String&) const;
    size_t rfind(const String&) const;
    String substr(size_t i, size_t count) const;
};

//
String::String(size_t n) : size(0), capacity(n ? n : DEFAULT_CAP) {
    backing = new char[capacity];
}

//
String::String(const char* s) {
    size     = std::strlen(s);
    capacity = size * 2;

    backing = new char[capacity];
    std::strncpy(backing, s, size);
}

String::String(const char c) : String(1, c) {}

//
String::String(size_t n, char c) : size(n), capacity(2 * n) {
    backing = new char[capacity];
    std::memset(backing, c, size);
}

//
String::String(const String& s) : size(s.size), capacity(2 * s.capacity) {
    backing = new char[capacity];
    std::strncpy(backing, s.backing, size);
}

//
String::~String() { delete[] backing; }

//
String& String::operator=(const String& rhs) {
    if (this == &rhs) {
        return *this;
    }

    delete[] backing;
    backing = new char[rhs.capacity];
    std::strncpy(backing, rhs.backing, rhs.size);

    size     = rhs.size;
    capacity = rhs.capacity;

    return *this;
}

//
void String::push_back(char c) { *this += c; }

//
void String::pop_back() { size--; }

//
std::ostream& operator<<(std::ostream& os, const String& s) {
    for (size_t i = 0; i < s.size; i++) {
        os << s.backing[i];
    }
    return os;
}

//
std::istream& operator>>(std::istream& is, String& s) {
    // TODO: ask if clear is needed
    s.clear();

    char c;
    while (is.get(c)) {
        if (std::isspace(c))
            return is;

        s.push_back(c);
    }
    return is;
}

//
String& String::operator+=(const String& rhs) {
    if (size + rhs.size >= capacity) {
        capacity = 2 * (size + rhs.size);

        char* tmp = new char[capacity];

        std::strncpy(tmp, backing, size);
        delete[] backing;
        backing = tmp;
    }

    std::strncpy(backing + size, rhs.backing, rhs.size);
    size += rhs.size;

    return *this;
}

String& String::operator+=(const char c) {
    push_back(c);
    return *this;
}

//?
String operator+(const String& lhs, const String& rhs) {
    String s(lhs.size + rhs.size);
    s += lhs;
    s += rhs;

    return s;
}

//
void String::clear() { size = 0; }

//?
bool operator==(const String& lhs, const String& rhs) {
    if (lhs.size != rhs.size)
        return false;

    return std::strncmp(lhs.backing, rhs.backing, lhs.size) == 0;
}

//?
String String::substr(size_t i, size_t count) const {
    // assert(i <= size - 1)
    /*
    if (size - i < count) {
        count -= count - size + i;
    }
    */

    char* s = new char[count];
    std::strncpy(s, backing + i, count);

    return s;
}

size_t String::find(const String& pattern) const {
    size_t i = 0;
    while (i <= size - pattern.size) {
        if ((*this)[i] == pattern.front()) {
            if (std::strncmp(backing + i, pattern.backing, pattern.size) == 0)
                return i;
        }

        ++i;
    }

    return size;
}

// TODO: ask if we can use strstr
size_t String::rfind(const String& pattern) const {
    size_t i = size - pattern.size;

    while (i > 0) {
        if ((*this)[i] == pattern.front()) {
            if (std::strncmp(backing + i, pattern.backing, pattern.size) == 0)
                return i;
        }

        --i;
    }

    if (std::strncmp(backing, pattern.backing, pattern.size) == 0)
        return 0;

    return size;
}
