#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class BigInteger {
    friend std::ostream& operator<<(std::ostream&, const BigInteger&);
    friend std::istream& operator>>(std::istream&, BigInteger&);

    friend bool operator<(const BigInteger& lhs, const BigInteger& rhs);

    std::vector<int> backing;
    bool             negative;

    bool is_zero() const { return backing.size() == 1 && backing[0] == 0; }
    void pop_zeroes();

  public:
    static const int BASE        = 10000;
    static const int BASE_LOG_10 = 4;

    BigInteger() : negative(false) { backing.push_back(0); }
    BigInteger(int);
    explicit BigInteger(const std::string&);

    BigInteger operator-() const;

    BigInteger& operator+=(const BigInteger& rhs);
    BigInteger& operator-=(const BigInteger& rhs);

    BigInteger& operator*=(const BigInteger& rhs);
    BigInteger& operator/=(const BigInteger& rhs);
    BigInteger& operator%=(const BigInteger& rhs);

    BigInteger& operator++() { return *this += 1; }
    BigInteger& operator--() { return *this -= 1; }

    BigInteger operator++(int) {
        BigInteger temp = *this;
        ++*this;
        return temp;
    }
    BigInteger operator--(int) {
        BigInteger temp = *this;
        ++*this;
        return temp;
    }

    std::string toString() const;
    void        flip_sign() { negative = !negative; }
    bool        is_negative() const { return negative; }

    explicit operator bool() const { return !is_zero(); }

    BigInteger abs() const {
        if (*this < 0)
            return -(*this);
        return *this;
    }
};

bool operator<(const BigInteger& lhs, const BigInteger& rhs) {
    if (lhs.negative != rhs.negative) {
        return lhs.negative;
    }
    if (rhs.backing.size() != lhs.backing.size()) {
        if (lhs.negative)
            return lhs.backing.size() > rhs.backing.size();
        return lhs.backing.size() < rhs.backing.size();
    }
    for (int i = lhs.backing.size() - 1; i >= 0; --i) {
        if (lhs.backing[i] != rhs.backing[i]) {
            if (lhs.negative)
                return lhs.backing[i] > rhs.backing[i];
            return lhs.backing[i] < rhs.backing[i];
        }
    }
    return false;
}

bool operator>(const BigInteger& lhs, const BigInteger& rhs) {
    return rhs < lhs;
}

bool operator==(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}
bool operator!=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs == rhs);
}

bool operator<=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs > rhs);
}
bool operator>=(const BigInteger& lhs, const BigInteger& rhs) {
    return !(lhs < rhs);
}

BigInteger::BigInteger(const std::string& sss) : negative(false) {
    std::string str = sss;
    if (str.empty()) {
        backing.push_back(0);
        return;
    }

    if (str[0] == '-') {
        negative = true;
        str[0]   = '0';
    }

    for (int i = str.size(); i > 0;) {
        size_t i_next = std::max(i - BASE_LOG_10, 0);

        backing.push_back(std::stoi(str.substr(i_next, i - i_next)));

        i = i_next;
    }

    pop_zeroes();
}

BigInteger::BigInteger(int val) : negative(val < 0) {
    if (negative)
        val *= -1;

    while (val > 0) {
        backing.push_back(val % BASE);
        val /= BASE;
    }

    if (backing.size() == 0)
        backing.push_back(0);
}

void BigInteger::pop_zeroes() {
    while (backing.size() > 1 && backing.back() == 0) {
        backing.pop_back();
    }
}

std::ostream& operator<<(std::ostream& os, const BigInteger& rhs) {
    if (rhs.is_zero())
        return os << 0;

    if (rhs.negative)
        os << "-";

    std::vector<int>::const_reverse_iterator iter = rhs.backing.rbegin();

    if (*iter)
        os << *iter;

    iter++;

    for (; iter != rhs.backing.rend(); iter++) {
        os.width(BigInteger::BASE_LOG_10);
        os.fill('0');
        os << *iter;
    }
    return os;
}

std::istream& operator>>(std::istream& is, BigInteger& rhs) {
    std::string str;
    is >> str;

    BigInteger n(str);
    rhs = n;

    return is;
}

std::string BigInteger::toString() const {
    std::string str;

    if (is_zero())
        return str = "0";

    if (negative)
        str += '-';

    std::vector<int>::const_reverse_iterator iter = backing.rbegin();
    if (*iter)
        str += std::to_string(*iter);

    iter++;

    for (; iter != backing.rend(); iter++) {
        std::string tmp = std::to_string(*iter);
        str += std::string(BASE_LOG_10, '0')
                   .replace(BASE_LOG_10 - tmp.size(), tmp.size(), tmp);
    }

    return str;
}

BigInteger BigInteger::operator-() const {
    BigInteger x = *this;
    if (x.is_zero())
        return x;

    x.negative = !x.negative;
    return x;
}

BigInteger& BigInteger::operator+=(const BigInteger& rhs) {
    if (negative != rhs.negative) {
        negative = !negative;
        *this -= rhs;
        negative = !negative;

        pop_zeroes();
        return *this;
    }

    size_t _size = std::max(backing.size() + 1, rhs.backing.size() + 1);
    backing.resize(_size, 0);

    int more = 0;
    for (size_t i = rhs.backing.size() - 1; i < rhs.backing.size(); --i) {
        backing[i] += rhs.backing[i] + more;

        more = backing[i] / BASE;
        backing[i] -= more * BASE;

        for (int j = 1; more; ++j) {
            backing[i + j] += more;
            more = backing[i + j] / BASE;
            backing[i + j] -= more * BASE;
        }
    }

    pop_zeroes();
    return *this;
}

BigInteger operator+(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result += rhs;
}

BigInteger& BigInteger::operator-=(const BigInteger& rhs) {
    if (*this == rhs)
        return *this = 0;

    if (negative != rhs.negative) {
        negative = !negative;
        *this += rhs;
        negative = !negative;

        return *this;
    }

    const BigInteger* bigger_ptr  = &rhs;
    const BigInteger* smaller_ptr = &rhs;

    if (!negative) {
        if (rhs > *this) {
            bigger_ptr = this;
            negative   = true;
        } else {
            smaller_ptr = this;
        }
    } else {
        if (rhs < *this) {
            bigger_ptr = this;
            negative   = false;
        } else {
            smaller_ptr = this;
        }
    }

    while (backing.size() < rhs.backing.size())
        backing.push_back(0);

    int per = 0;
    for (size_t i = 0; i < bigger_ptr->backing.size() || per == 1; ++i) {
        backing[i] =
            (i < bigger_ptr->backing.size())
                ? smaller_ptr->backing[i] - (bigger_ptr->backing[i] + per)
                : smaller_ptr->backing[i] - (per);
        per = 0;
        if (backing[i] < 0) {
            backing[i] += BASE;
            per = 1;
        }
    }

    pop_zeroes();
    return *this;
}

BigInteger operator-(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result -= rhs;
}

BigInteger& BigInteger::operator*=(const BigInteger& rhs) {
    BigInteger temp(0);

    if (rhs.is_zero() || (*this).is_zero())
        return *this = temp;

    temp.backing.resize(backing.size() + rhs.backing.size() + 1, 0);

    for (size_t i = backing.size() - 1; i <= backing.size(); --i) {
        for (size_t j = rhs.backing.size() - 1; j <= rhs.backing.size(); --j) {
            temp.backing[i + j] += (backing[i] * rhs.backing[j]);
            int per = temp.backing[i + j] / BASE;

            temp.backing[i + j] %= BASE;

            ssize_t per_index = i + j + 1;

            while (per > 0) {
                temp.backing[per_index] += per % BASE;
                per = temp.backing[per_index] / BASE;
                temp.backing[per_index] %= BASE;
                ++per_index;
            }
        }
    }

    //                    a.k.a xor
    temp.negative = negative != rhs.negative;
    temp.pop_zeroes();

    return *this = temp;
}

BigInteger operator*(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result *= rhs;
}

BigInteger& BigInteger::operator/=(const BigInteger& rhs) {
    BigInteger answer = 0;
    if (!rhs || !(*this))
        return *this = 0;

    BigInteger tmp = rhs;
    tmp.negative   = false;

    BigInteger helper2 = tmp;
    BigInteger helper1 = (*this);

    helper1.negative = false;

    BigInteger prev_s, prev_d, degree;
    while (helper1 >= helper2) {
        degree = 1;

        prev_s = helper2;
        prev_d = degree;

        while (helper1 >= helper2) {
            prev_s = helper2;
            prev_d = degree;

            helper2 *= BASE;
            degree *= BASE;
        }

        helper2 = prev_s;
        degree  = prev_d;

        while (helper1 >= helper2) {
            helper1 -= helper2;
            answer += degree;
        }

        helper2 = tmp;
        helper2.pop_zeroes();
    }

    answer.negative = (negative != rhs.negative);
    answer.pop_zeroes();

    return (*this) = answer;
}

BigInteger operator/(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result /= rhs;
}

BigInteger& BigInteger::operator%=(const BigInteger& rhs) {
    return *this -= rhs * ((*this) / rhs);
}

BigInteger operator%(const BigInteger& lhs, const BigInteger& rhs) {
    BigInteger result = lhs;
    return result %= rhs;
}

inline BigInteger gcd(BigInteger x, BigInteger y) {
    while (x && y) {
        if (x > y)
            x %= y;
        else
            y %= x;
    }
    return x + y;
}

class Rational {
    friend bool operator<(const Rational& lhs, const Rational& rhs);

    static const size_t DOUBLE_PRECISION = 32;
    BigInteger          num, den;

    void simplify();

  public:
    Rational(const BigInteger&, const BigInteger&);
    explicit Rational(const BigInteger& val) : Rational(val, 1) {}
    explicit Rational(int val) : Rational(BigInteger(val), 1) {}
    Rational() : Rational(0) {}

    Rational operator-() const;

    Rational& operator+=(const Rational& rhs);
    Rational& operator-=(const Rational& rhs);

    Rational& operator*=(const Rational& rhs);
    Rational& operator/=(const Rational& rhs);

    std::string toString() const;
    std::string asDecimal(size_t precision = 0) const;

    explicit operator double() const {
        return std::stod(asDecimal(DOUBLE_PRECISION));
    }
};

void Rational::simplify() {
    BigInteger com;
    if (num < 0)
        com = gcd(-num, den);
    else
        com = gcd(num, den);

    num /= com;
    den /= com;
}

Rational::Rational(const BigInteger& num, const BigInteger& den_) : num(num) {
    if (den < 0)
        den = -den_;
    else
        den = den_;
    simplify();
}

Rational Rational::operator-() const {
    Rational result = *this;
    result.num.flip_sign();
    return result;
}

Rational& Rational::operator+=(const Rational& rhs) {
    num = num * rhs.den + rhs.num * den;
    den *= rhs.den;
    simplify();
    return *this;
}

Rational& Rational::operator-=(const Rational& rhs) {
    num = num * rhs.den - rhs.num * den;
    den *= rhs.den;
    simplify();
    return *this;
}

Rational& Rational::operator*=(const Rational& rhs) {
    num *= rhs.num;
    den *= rhs.den;
    simplify();

    return *this;
}

Rational& Rational::operator/=(const Rational& rhs) {
    if (rhs.num == 0)
        throw "zero division";

    num *= rhs.den;
    den *= rhs.num;

    if (den < 0) {
        den.flip_sign();
        num.flip_sign();
    }

    simplify();

    return *this;
}

std::string Rational::toString() const {
    std::string str;
    str += num.toString();

    if (den != 1)
        str += '/' + den.toString();

    return str;
}

std::string Rational::asDecimal(size_t precision) const {
    std::string str;
    BigInteger  n = num, d = den;

    if (num == 0)
        return "0";
    if (num < 0) {
        str += '-';
        n.flip_sign();
    }

    str += (n / d).toString();
    if (precision == 0)
        return str;

    str += '.';

    n %= d;

    for (size_t i = 0; i < precision; i++) {
        n *= 10;
        str += (n / d).toString();
        n %= d;
    }

    return str;
}

bool operator<(const Rational& lhs, const Rational& rhs) {
    return lhs.num * rhs.den < rhs.num * lhs.den;
}

bool operator>(const Rational& lhs, const Rational& rhs) { return (rhs < lhs); }

bool operator==(const Rational& lhs, const Rational& rhs) {
    return !(lhs < rhs) && !(rhs < lhs);
}

bool operator!=(const Rational& lhs, const Rational& rhs) {
    return !(lhs == rhs);
}

bool operator<=(const Rational& lhs, const Rational& rhs) {
    return !(lhs > rhs);
}

bool operator>=(const Rational& lhs, const Rational& rhs) {
    return !(lhs < rhs);
}

Rational operator+(const Rational& lhs, const Rational& rhs) {
    Rational result = lhs;
    return result += rhs;
}
Rational operator-(const Rational& lhs, const Rational& rhs) {
    Rational result = lhs;
    return result -= rhs;
}
Rational operator*(const Rational& lhs, const Rational& rhs) {
    Rational result = lhs;
    return result *= rhs;
}
Rational operator/(const Rational& lhs, const Rational& rhs) {
    Rational result = lhs;
    return result /= rhs;
}
