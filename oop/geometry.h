#include <algorithm>
#include <cmath>
#include <cstdarg>
#include <functional>
#include <iostream>
#include <numeric>
#include <typeinfo>
#include <utility>
#include <vector>

const double EPS = 1e-5;

inline bool double_cmp(const double& lhs, const double& rhs) {
    return std::fabs(lhs - rhs) < EPS;
}

template <typename InputIt1, typename InputIt2>
constexpr bool is_cyclic_permutation(InputIt1 first1,
                                     InputIt1 last1,
                                     InputIt2 first2,
                                     InputIt2 last2) {
    if (std::distance(first1, last1) != std::distance(first2, last2))
        return false;

    // find element from which second's suffix matches first's prefix
    // and check if such an element was found
    InputIt2 fst_match2 = first2;
    while (fst_match2 != last2) {
        fst_match2 = std::find(fst_match2, last2, *first1);
        if (fst_match2 == last2)
            return false;

        if (std::equal(fst_match2, last2, first1)) {
            break;
        }

        fst_match2++;
    }

    // check if second's prefix matches first's suffix
    return std::equal(
        first2, fst_match2, first1 + std::distance(fst_match2, last2));
}

class Line;

// Describes a point in space
class Point {
  public:
    double x, y;

    Point(double x, double y) : x(x), y(y){};

    Point& operator+=(const Point& rhs) {
        x += rhs.x;
        y += rhs.y;
        return *this;
    }

    Point& operator-=(const Point& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    Point& operator*=(double scalar) {
        x *= scalar;
        y *= scalar;
        return *this;
    }

    // return opposite vector
    Point operator-() const { return {-x, -y}; }

    Point& rotate(const Point&, double angle);
    Point& reflex(const Point&);
    Point& reflex(const Line&);
};

inline std::ostream& operator<<(std::ostream& os, const Point& p) {
    return os << '(' << p.x << ", " << p.y << ')';
}

inline Point operator+(const Point& lhs, const Point& rhs) {
    Point result = lhs;
    return result += rhs;
}

inline Point operator-(const Point& lhs, const Point& rhs) {
    Point result = lhs;
    return result -= rhs;
}

inline Point operator*(const Point& lhs, double scalar) {
    Point result = lhs;
    return result *= scalar;
}

inline bool operator==(const Point& lhs, const Point& rhs) {
    return double_cmp(lhs.x, rhs.x) && double_cmp(lhs.y, rhs.y);
}

inline bool operator!=(const Point& lhs, const Point& rhs) {
    return !(lhs == rhs);
}

// computes vector product of x and y
inline double vectorprod(const Point& lhs, const Point& rhs) {
    return lhs.x * rhs.y - lhs.y * rhs.x;
}

// norm calculates the norm of the vector OP, where O is the
// the (0, 0) point and P a given point
inline double norm(const Point& p) { return sqrt(p.x * p.x + p.y * p.y); }

// compute the euclidean distance between two points
inline double euc_dist(const Point& p1, const Point& p2) {
    return norm(p1 - p2);
}

// returns unit vector corresponding to the vector OP, where O is
// the (0, 0) point and P a given point
inline Point normalized(const Point& p) { return p * (1 / norm(p)); }

// rotate self around `p` point by angle `angle`
inline Point& Point::rotate(const Point& p, double angle) {
    angle = (angle * M_PI) / 180;

    *this -= p;

    double upd_x = x * cos(angle) - y * sin(angle);
    double upd_y = y * cos(angle) + x * sin(angle);

    x = upd_x;
    y = upd_y;

    return *this += p;
}

inline Point& Point::reflex(const Point& p) {
    return (*this) += (p - *this) * 2;
}

enum Orientation { leftof = 1, on = 0, rightof = -1 };

// Line $$ a*x + b*y + c = 0 $$
class Line {
    friend Point         intersection(Line l1, Line l2);
    friend std::ostream& operator<<(std::ostream&, const Line&);

    double a, b, c;

  public:
    Line(double k, double b) : a(-k), b(1), c(-b) {}
    Line(const Point& p, double k) { Line(k, p.y - k * p.x); }
    Line(const Point& fst, const Point& snd) :
        a(fst.y - snd.y), b(snd.x - fst.x) {
        c = -(a * fst.x + b * fst.y);
    }

    bool operator==(const Line& rhs) {
        return double_cmp(a * rhs.b, b * rhs.a) &&
               double_cmp(a * rhs.c, c * rhs.a);
    }
    bool operator!=(const Line& rhs) { return !(*this == rhs); }

    // creates a perpendicular line at given point
    Line perpendicular(Point) const;

    Orientation side(const Point& p) {
        double eval = a * p.x + b * p.y + c;

        if (double_cmp(eval, 0.))
            return Orientation::on;

        return (eval > 0.) ? Orientation::leftof : Orientation::rightof;
    }
};

// returns projection of point onto line
inline Point project(const Point& p, const Line& l) {
    return intersection(l.perpendicular(p), l);
}

inline Point& Point::reflex(const Line& l) {
    return *this += (project(*this, l) - *this) * 2;
}

inline Line Line::perpendicular(Point p) const {
    return Line(p, p + Point{a, b});
}

// calculate the intersection of two lines
inline Point intersection(Line l1, Line l2) {
    // solve using cramer's rule
    double delta   = l1.a * l2.b - l2.a * l1.b;
    double delta_1 = -l1.c * l2.b + l2.c * l1.b;
    double delta_2 = -l1.a * l2.c + l2.a * l1.c;

    return {delta_1 / delta, delta_2 / delta};
}

// creates a line bisecting angle generated by given points
inline Line bisection(const Point& p1, const Point& p2, const Point& p3) {
    Point vec1 = normalized(p1 - p2);
    Point vec2 = normalized(p3 - p2);
    return Line(p2, p2 + vec1 + vec2);
}

// finds the midpoint of segment generated by given points
inline Point midpoint(const Point& p1, const Point& p2) {
    return (p1 + p2) * 0.5;
}

// creates a line passing through middle and perpendicular
// to segment generated by given points
inline Line midperp(const Point& p1, const Point& p2) {
    return Line(p1, p2).perpendicular(midpoint(p1, p2));
}

// comput the dot product between two vectors
inline double dot(const Point& p1, const Point& p2) {
    return p1.x * p2.x + p1.y * p2.y;
}

// compute the cosine between two vectors
inline double cos(const Point& p1, const Point& p2) {
    return dot(p1, p2) / norm(p1) / norm(p2);
}

inline std::ostream& operator<<(std::ostream& os, const Line& line) {
    return os << line.a << "x + " << line.b << "y +" << line.c << " = 0";
}

// Abstract shape class
class Shape {
  public:
    virtual bool operator==(const Shape& another) const = 0;
    virtual bool operator!=(const Shape& another) const = 0;

    virtual double perimeter() const = 0;
    virtual double area() const      = 0;

    virtual bool isCongruentTo(const Shape& another) const = 0;
    virtual bool isSimilarTo(const Shape& another) const   = 0;
    virtual bool containsPoint(const Point& point) const   = 0;

    virtual void rotate(const Point&, double angle) = 0;
    virtual void reflex(const Point&)               = 0;
    virtual void reflex(const Line&)                = 0;
    virtual void scale(const Point&, double factor) = 0;

    virtual ~Shape() = default;
};

class Polygon : public Shape {
    // cpplit ignore
  protected:
    std::vector<Point> vertices;

  public:
    friend std::ostream& operator<<(std::ostream&, const Polygon&);

    explicit Polygon(const std::vector<Point>& verts) : vertices(verts) {}
    Polygon(std::initializer_list<Point> vertices) : vertices(vertices) {}

    std::vector<Point>  sideVectors() const;
    std::vector<double> sideLengths() const;
    std::vector<double> angles() const;

    size_t      vecticesCount() const { return vertices.size(); }
    const auto& getVertices() const { return vertices; }

    bool isConvex() const;
    bool operator==(const Shape&) const override;
    bool operator!=(const Shape&) const override;
    bool isCongruentTo(const Shape&) const override;
    bool isSimilarTo(const Shape&) const override;
    bool containsPoint(const Point&) const override;

    double perimeter() const override {
        // calculate the sum of norms of vectors
        // generated by adjacent vertices
        return std::inner_product(
            vertices.begin(),
            vertices.end() - 1,
            vertices.begin() + 1,
            euc_dist(vertices.back(), vertices.front()),
            std::plus<double>(),
            [](const auto& fst, const auto& snd) -> double {
                return euc_dist(snd, fst);
            });
    }

    // calculate area using this formula:
    // https://en.wikipedia.org/wiki/Polygon#Area
    double area() const override {
        return 0.5 * std::abs(std::inner_product(
                         vertices.begin(),
                         vertices.end() - 1,
                         vertices.begin() + 1,
                         vectorprod(vertices.back(), vertices.front()),
                         std::plus<double>(),
                         vectorprod));
    }

    void rotate(const Point& pivot, double angle) override {
        std::for_each(vertices.begin(),
                      vertices.end(),
                      [&pivot, &angle](Point& p) { p.rotate(pivot, angle); });
    }

    void reflex(const Point& pivot) override {
        std::for_each(vertices.begin(), vertices.end(), [&pivot](Point& p) {
            p.reflex(pivot);
        });
    }

    void reflex(const Line& l) override {
        std::for_each(
            vertices.begin(), vertices.end(), [&l](Point& p) { p.reflex(l); });
    }

    void scale(const Point& center, double factor) override {
        std::transform(vertices.begin(),
                       vertices.end(),
                       vertices.begin(),
                       [&center, &factor](const Point& p) -> Point {
                           return center + (p - center) * factor;
                       });
    }

    virtual ~Polygon() {}
};

inline std::ostream& operator<<(std::ostream& os, const Polygon& pol) {
    os << '{';
    for (const Point& p : pol.vertices) {
        os << p << ", ";
    }
    return os << '}';
}

inline std::vector<Point> Polygon::sideVectors() const {
    std::vector<Point> vecs;
    std::transform(
        vertices.begin() + 1,
        vertices.end(),
        vertices.begin(),
        std::back_inserter(vecs),
        [](const Point& nxt, const Point& ths) { return nxt - ths; });
    vecs.push_back(vertices.front() - vertices.back());

    return vecs;
}

inline std::vector<double> Polygon::sideLengths() const {
    std::vector<double> lengths;
    std::transform(vertices.begin() + 1,
                   vertices.end(),
                   vertices.begin(),
                   std::back_inserter(lengths),
                   euc_dist);
    lengths.push_back(euc_dist(vertices.front(), vertices.back()));

    return lengths;
}

inline std::vector<double> Polygon::angles() const {
    auto                vecs = sideVectors();
    std::vector<double> angles{cos(-vecs.back(), vecs.front())};
    std::transform(
        vecs.begin() + 1,
        vecs.end(),
        vecs.begin(),
        std::back_inserter(angles),
        [](const Point& v1, const Point& v2) { return cos(v1, -v2); });

    return angles;
}

inline bool Polygon::isConvex() const {
    size_t n_vert = vertices.size();

    for (size_t i = 0; i < n_vert; i++) {
        Line side = Line(vertices[i], vertices[(i + 1) % n_vert]);
        if (side.side(vertices[(n_vert + i - 1) % n_vert]) !=
            side.side(vertices[(i + 2) % n_vert]))
            return false;
    }

    return true;
}

inline bool Polygon::operator==(const Shape& shape) const {
    // polygons are equal if their vertex lists
    // are cyclic permutations or cyclic permutations of
    // reverse of each other

    try {
        const Polygon& rhs = dynamic_cast<const Polygon&>(shape);

        return is_cyclic_permutation(vertices.begin(),
                                     vertices.end(),
                                     rhs.vertices.begin(),
                                     rhs.vertices.end()) ||
               is_cyclic_permutation(vertices.rbegin(),
                                     vertices.rend(),
                                     rhs.vertices.begin(),
                                     rhs.vertices.end());
    } catch (const std::bad_cast& e) {
        return false;
    }
}

inline bool Polygon::operator!=(const Shape& rhs) const {
    return !(*this == rhs);
}

inline bool Polygon::isCongruentTo(const Shape& shape) const {
    try {
        const Polygon& that = dynamic_cast<const Polygon&>(shape);

        if (vertices.size() != that.vertices.size())
            return false;

        size_t size = vertices.size();

        const std::vector<double> this_angles = angles();
        const std::vector<double> that_angles = that.angles();
        const std::vector<double> this_sides  = sideLengths();
        const std::vector<double> that_side   = that.sideLengths();

        for (size_t i = 0; i < size; i++) {
            bool congr = true;
            for (size_t j = 0; j < size && congr; j++)
                if (!double_cmp(this_angles[(i + j) % size], that_angles[j]) ||
                    !double_cmp(this_sides[i + j % size], that_side[j])) {
                    congr = false;
                }

            if (congr)
                return true;
        }

        for (size_t i = 0; i < size; i++) {
            bool congr = true;
            for (size_t j = 0; j < size && congr; j++)
                if (!double_cmp(this_angles[(i + j) % size],
                                that_angles[size - j - 1]) ||
                    !double_cmp(this_sides[(i + j) % size],
                                that_side[(2 * size - j - 2) % size])) {
                    congr = false;
                }

            if (congr)
                return true;
        }

        return false;
    } catch (const std::bad_cast& _) {
        return false;
    }
}

inline bool Polygon::isSimilarTo(const Shape& shape) const {
    try {
        const Polygon& that = dynamic_cast<const Polygon&>(shape);

        double p_rat = that.perimeter() / perimeter();

        auto this_cp = *this;
        this_cp.scale({0, 0}, p_rat);

        return this_cp.isCongruentTo(that);
    } catch (const std::bad_cast& _) {
        return false;
    }
}

inline double angle(const Point& a, const Point& b) {
    double phi   = std::atan2(a.y, a.x);
    double theta = std::atan2(b.y, b.x);

    double delta = phi - theta;

    while (delta > M_PI)
        delta -= 2 * M_PI;

    while (delta < -M_PI)
        delta += 2 * M_PI;

    return delta;
}

inline bool Polygon::containsPoint(const Point& p) const {
    auto triangulate = [&p](const Point& p1, const Point& p2) {
        return angle(p1 - p, p2 - p);
    };

    double angle_sum =
        std::inner_product(vertices.begin(),
                           vertices.end() - 1,
                           vertices.begin() + 1,
                           triangulate(vertices.back(), vertices.front()),
                           std::plus<double>(),
                           triangulate);

    if (double_cmp(std::abs(angle_sum), M_PI * 2))
        return true;

    return false;
}

class Rectangle : public Polygon {

  public:
    Rectangle(const Point& p1, const Point& p2, double aspRat) : Polygon({p1}) {
        Point center = midpoint(p1, p2);

        Point p3 = p2;
        p3.rotate(center, 2 * atan(aspRat));

        Point p4 = p3;
        p4.reflex(center);

        vertices = std::vector<Point>{p1, p3, p2, p4};
    }

    Point center() const { return (vertices[0] + vertices[2]) * 0.5; }

    std::pair<Line, Line> diagonals() const {
        return std::make_pair(Line(vertices[0], vertices[2]),
                              Line(vertices[1], vertices[3]));
    }
};

class Ellipse : public Shape {
  protected:
    Point  f1, f2;
    double dist;

  public:
    Ellipse(const Point& f1, const Point& f2, double dist) :
        f1(f1), f2(f2), dist(dist) {}

    std::pair<Point, Point> focuses() const { return std::make_pair(f1, f2); }
    double eccentricity() const { return euc_dist(f1, f2) / dist; }
    Point  center() const { return midpoint(f1, f2); }
    std::pair<Line, Line> directrices() const;

    bool operator==(const Shape&) const override;
    bool operator!=(const Shape&) const override;
    bool isCongruentTo(const Shape&) const override;
    bool isSimilarTo(const Shape&) const override;
    bool containsPoint(const Point&) const override;

    double area() const override {
        const double focal_dist = euc_dist(f1, f2);
        // distance from center to intersection
        // of line connecting focuses and ellipse
        const double a = dist / 2.;
        const double b =
            std::sqrt(std::pow(dist, 2) / 4 - std::pow(focal_dist, 2) / 4);
        return M_PI * a * b;
    }

    double perimeter() const override {
        const double focal_dist = euc_dist(f1, f2);
        // distance from center to intersection
        // of line connecting focuses and ellipse
        const double a = dist / 2.;
        const double b =
            std::sqrt(std::pow(dist, 2) / 4 - std::pow(focal_dist, 2) / 4);
        return M_PI * (3 * (a + b) - std::sqrt((3 * a + b) * (a + 3 * b)));
    }

    void rotate(const Point& pivot, double angle) override {
        f1.rotate(pivot, angle);
        f2.rotate(pivot, angle);
    }

    void reflex(const Point& p) override {
        f1.reflex(p);
        f2.reflex(p);
    }

    void reflex(const Line& l) override {
        f1.reflex(l);
        f2.reflex(l);
    }

    void scale(const Point& p, double factor) override {
        f1 = p + (f1 - p) * factor;
        f2 = p + (f2 - p) * factor;
        dist *= factor;
    }
};

inline bool Ellipse::operator==(const Shape& rhs) const {
    try {
        const Ellipse& ellipse = dynamic_cast<const Ellipse&>(rhs);
        return f1 == ellipse.f1 && f2 == ellipse.f2 &&
               double_cmp(dist, ellipse.dist);
    } catch (const std::bad_cast& _) {
        return false;
    }
}

inline bool Ellipse::containsPoint(const Point& p) const {
    return euc_dist(f1, p) + euc_dist(f2, p) < dist;
}

inline bool Ellipse::isCongruentTo(const Shape& shape) const {
    try {
        const Ellipse& rhs = dynamic_cast<const Ellipse&>(shape);
        return double_cmp(euc_dist(f1, f2), euc_dist(rhs.f1, rhs.f2)) &&
               double_cmp(dist, rhs.dist);
    } catch (const std::bad_cast& _) {
        return false;
    }
}

inline bool Ellipse::isSimilarTo(const Shape& shape) const {
    // WHERE THE ISINSTANCE AT?
    try {
        const Ellipse& rhs         = dynamic_cast<const Ellipse&>(shape);
        double         this_f_dist = euc_dist(f1, f2);
        double         that_f_dist = euc_dist(rhs.f1, rhs.f2);

        if (double_cmp(this_f_dist, 0.) != double_cmp(that_f_dist, 0.))
            return false;
        else if (double_cmp(this_f_dist, 0.) && double_cmp(that_f_dist, 0.))
            return true;

        return double_cmp(this_f_dist / that_f_dist, dist / rhs.dist);
    } catch (const std::bad_cast& _) {
        return false;
    }
}

inline bool Ellipse::operator!=(const Shape& rhs) const {
    return !(*this == rhs);
}

inline std::pair<Line, Line> Ellipse::directrices() const {
    // distance between focal points
    double f_dist = euc_dist(f1, f2);
    // from focus point to closes point on ellipse
    double alpha = dist - f_dist;

    // dirictrices are perpendicular to the ellipse's center line.
    // to find the points they pass through we need to find a
    // points on the center line and are twice as distant
    // from the focal points as the closes point on the circle
    Line centerline = Line(f1, f2);
    return std::make_pair(
        centerline.perpendicular(f1 + (f1 - f2) * (2 * alpha / f_dist)),
        centerline.perpendicular(f2 + (f2 - f1) * (2 * alpha / f_dist)));
}

class Circle : public Ellipse {
  public:
    Circle(const Point& center, double radius) :
        Ellipse(center, center, radius) {}
    double radius() const { return dist; }
};

class Square : public Rectangle {
  public:
    Square(const Point& p1, const Point& p2) : Rectangle(p1, p2, 1) {}
    Circle circumscribedCircle() const {
        return Circle(center(), euc_dist(vertices[0], vertices[2]) / 2);
    }

    Circle inscribedCircle() const {
        return Circle(center(),
                      euc_dist(vertices[0], vertices[2]) / (2 * sqrt(2)));
    }
};

class Triangle : public Polygon {
  public:
    Triangle(const Point& p1, const Point& p2, const Point& p3) :
        Polygon({p1, p2, p3}) {}

    Circle inscribedCircle() const {
        return Circle(
            // center of circle is located on the
            // intercestion of angle bisectors
            intersection(bisection(vertices[0], vertices[1], vertices[2]),
                         bisection(vertices[1], vertices[2], vertices[0])),
            // r = S/p
            2 * area() / perimeter());
    }

    Circle circumscribedCircle() const {
        // center of circle is located on
        // intersection of midpoint perpendiculars
        auto center = intersection(midperp(vertices[0], vertices[1]),
                                   midperp(vertices[1], vertices[2]));
        return Circle(center, euc_dist(center, vertices[0]));
    }

    Point centroid() const {
        // std::cerr << *this;
        // calculate the intersection of lines passing
        // through vertex and middle of opposite side
        return intersection(
            Line(vertices[0], midpoint(vertices[1], vertices[2])),
            Line(vertices[1], midpoint(vertices[2], vertices[0])));
    }

    Point orthocenter() const {
        // calculate the intersection of lines passing
        // through vertex and perpendicular to opposite side (altitudes)
        return intersection(
            Line(vertices[0], vertices[1]).perpendicular(vertices[2]),
            Line(vertices[1], vertices[2]).perpendicular(vertices[0]));
    }

    Line EulerLine() const { return Line(orthocenter(), centroid()); }

    Circle ninePointsCircle() const {
        // the nine point circle
        // circumscribes circle generated by
        // side pidpoints
        return Triangle(midpoint(vertices[0], vertices[1]),
                        midpoint(vertices[1], vertices[2]),
                        midpoint(vertices[2], vertices[0]))
            .circumscribedCircle();
    }
};
